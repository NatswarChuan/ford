<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerContactController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use App\Models\Color;
use App\Models\Post;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('products/{slug}', [ProductController::class, 'getProductsBySlugAPI']);
Route::get('products', [ProductController::class, 'getProductsAPI']);

Route::get('contact', [PageController::class, 'contact']);

Route::post('thong-tin-khach-hang', [CustomerContactController::class, 'contactContomer'])->name('contactContomer');
Route::post('dang-ky-lai-thu', [CustomerContactController::class, 'testDrive'])->name('testDrive');
Route::post('dang-ky-tra-gop', [CustomerContactController::class, 'credit'])->name('credit');
Route::post('bao-gia', [CustomerContactController::class, 'quotation'])->name('bao-gia');
Route::post('tai-ebrochure', [CustomerContactController::class, 'ebrochure'])->name('ebrochure');

Route::get('/images', function () {
    $imagePath = public_path('storage');
    $images = collect(File::files($imagePath))
        ->sortBy(function ($file) {
            return $file->getCTime();
        })
        ->map(function ($file) {
            return $file->getBaseName();
        });
    $data = [];
    foreach ($images as $key => $item) {
  $data[] = $item;
}
    return response()->json(['images' => array_reverse($data)]);
});

Route::post('/images/uploads', function (Request $request) {
   if ($request->hasFile('image')) {
        $filename = basename($_FILES['image']['name']);
        move_uploaded_file($_FILES['image']['tmp_name'],public_path('/storage/' . $filename));
        return response()->json(['path' => Storage::url(public_path('/storage/' . $filename))], 200);
    }

    return response()->json(['error' => 'Không tìm thấy hình ảnh'], 400);
});

Route::post('/images/remove', function (Request $request) {
    $imagePath = $request->input('image');

    $sliders = Slider::where('image', $imagePath)->count();
    $colors = Color::where('image', $imagePath)->count();
    $posts = Post::where('image', $imagePath)->orWhere('content','like','%/storage/'.$imagePath.'%')->count();
    $product = Product::where('content','like','%/storage/'.$imagePath.'%')->count();
    if (!$sliders && !$colors && !$posts && !$product) {
        if ($imagePath) {
            $publicPath = public_path('/storage/' . $imagePath);

            if (file_exists($publicPath)) {
                unlink($publicPath);
                return response()->json(['success' => 'Hình ảnh đã được xóa thành công'], 200);
            } else {
                return response()->json(['error' => 'Không tìm thấy hình ảnh'], 404);
            }
        }
    }

    return response()->json(['error' => 'Hình ảnh đang được sử dụng, không thể xóa'], 400);
});
