<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerContactController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\UserController;
use App\Models\CustomerContact;

use Illuminate\Support\Facades\Artisan;

Route::get('/foo', function () {
    Artisan::call('storage:link');
});

/*
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PageController::class, 'index'])->name("home");

Route::get('/test', function(){
    return view('test');
});

Route::get('/admin/login', [LoginController::class, 'showLoginForm'])->middleware('login')->name('login');
Route::post('/admin/login', [LoginController::class, 'login']);
Route::post('/admin/logout', [LoginController::class, 'logout'])->middleware('auth')->name('logout');

Route::get('/san-pham', [ProductController::class, 'index'])->name('product.index');
Route::get('/san-pham/tra-gop', [ProductController::class, 'credit'])->name('product.credit');
Route::get('/san-pham/xe-cu', [ProductController::class, 'oldCar'])->name('product.oldCar');
Route::get('/san-pham/xe-cu/{slug}', [ProductController::class, 'getOldCarBySlug'])->name('product.oldCarShow');

Route::get('/san-pham/ebrochure', [ProductController::class, 'ebrochure'])->name('product.ebrochure');
Route::get('/san-pham/ebrochure/tai-ve/{slug}', [ProductController::class, 'downloadEbrochure'])->name('product.download');
Route::get('/chi-tiet-san-pham/{slug}', [ProductController::class, 'show'])->name('product.show');

Route::get('/tin-tuc-va-su-kien', [PostController::class, 'index'])->name('posts.index');
Route::get('/tin-tuc-va-su-kien/{slug}', [PostController::class, 'show'])->name('posts.show');
Route::get('/dich-vu', [PageController::class, 'service'])->name('page.service');
Route::get('/lien-he-voi-chung-toi', [PageController::class, 'aboutUs'])->name('page.aboutUs');

Route::get('/san-pham/so-sanh/{slug}', [ProductController::class, 'productEqual'])->name('product.equal');
Route::get('/san-pham/so-sanh', [ProductController::class, 'productEqualPage'])->name('product.equal.page');
Route::get('/san-pham/so-sanh/remove/{id}', [ProductController::class, 'removeProductEqual'])->name('product.equal.remove');

Route::middleware(['auth'])->group(function () { {
        Route::get('/admin', [PageController::class, 'admin'])->name('admin');
        Route::post('/admin', [StoreController::class, 'update'])->name('admin.store');
        Route::get('/admin/tin-tuc-va-su-kien', [PostController::class, 'adminIndex'])->name('admin.post');
        Route::get('/admin/tin-tuc-va-su-kien/{slug}/edit', [PostController::class, 'edit'])->name('admin.post.edit');
        Route::post('/admin/tin-tuc-va-su-kien/{slug}/edit', [PostController::class, 'update'])->name('admin.post.update');
        Route::post('/admin/tin-tuc-va-su-kien/{slug}/delete', [PostController::class, 'destroy'])->name('admin.post.destroy');
        Route::get('/admin/tin-tuc-va-su-kien/create', [PostController::class, 'create'])->name('admin.post.create');
        Route::post('/admin/tin-tuc-va-su-kien/create', [PostController::class, 'store'])->name('admin.post.store');

        Route::get('/admin/san-pham', [ProductController::class, 'adminIndex'])->name('admin.product');
        Route::get('/admin/san-pham/create', [ProductController::class, 'create'])->name('admin.product.create');

        Route::get('/admin/san-pham/xe-cu', [ProductController::class, 'adminOldCar'])->name('admin.product.oldCar');
        Route::post('/admin/san-pham/xe-cu/create', [ProductController::class, 'storeOldCar'])->name('admin.product.oldCarStore');
        Route::get('/admin/san-pham/xe-cu/create', [ProductController::class, 'createOldCar'])->name('admin.product.oldCarCreate');

        Route::get('/admin/san-pham/xe-cu/{slug}/edit', [ProductController::class, 'editOldCar'])->name('admin.product.oldCarEdit');
        Route::post('/admin/san-pham/xe-cu/{slug}/edit', [ProductController::class, 'updateOldCar'])->name('admin.product.oldCarUpdate');

        Route::post('/admin/san-pham/create', [ProductController::class, 'store'])->name('admin.product.store');
        Route::get('/admin/san-pham/{slug}/edit', [ProductController::class, 'edit'])->name('admin.product.edit');
        Route::post('/admin/san-pham/{slug}/edit', [ProductController::class, 'update'])->name('admin.product.update');
        Route::post('/admin/san-pham/{slug}/delete', [ProductController::class, 'destroy'])->name('admin.product.destroy');
        Route::get('/admin/san-pham/dong-san-pham/{id}', [ProductController::class, 'getByProductId'])->name('admin.product.category');

        Route::get('/admin/dong-san-pham', [CategoryController::class, 'adminIndex'])->name('admin.category');
        Route::post('/admin/dong-san-pham/create', [CategoryController::class, 'create'])->name('admin.category.create');
        Route::post('/admin/dong-san-pham/{id}/delete', [CategoryController::class, 'destroy'])->name('admin.category.destroy');
        Route::post('/admin/dong-san-pham/{id}/update', [CategoryController::class, 'update'])->name('admin.category.update');

        Route::get('/admin/thong-tin-khach-hang', [CustomerContactController::class, 'index'])->name('admin.customer');
        Route::post('/admin/thong-tin-khach-hang/{id}/delete', [CustomerContactController::class, 'destroy'])->name('admin.customer.destroy');
        Route::get('/admin/thong-tin-khach-hang/xuat-file', [CustomerContactController::class, 'exportContact'])->name('customer.export.contact');

        Route::get('/admin/thong-tin-khach-hang/khach-hang-lai-thu', [CustomerContactController::class, 'showTestDrive'])->name('admin.customer.test');
        Route::get('/admin/thong-tin-khach-hang/khach-hang-lai-thu/xuat-file', [CustomerContactController::class, 'exportTestDrive'])->name('admin.export.test');

        Route::get('/admin/thong-tin-khach-hang/khach-hang-bao-gia', [CustomerContactController::class, 'showQuotation'])->name('admin.customer.price');
        Route::get('/admin/thong-tin-khach-hang/khach-hang-bao-gia/xuat-file', [CustomerContactController::class, 'exportQuotation'])->name('admin.export.price');

        Route::get('/admin/thong-tin-khach-hang/khach-hang-tai-ebrochure', [CustomerContactController::class, 'showEbrochure'])->name('admin.customer.ebrochure');
        Route::get('/admin/thong-tin-khach-hang/khach-hang-tai-ebrochure/xuat-file', [CustomerContactController::class, 'exportEbrochure'])->name('admin.export.ebrochure');

        Route::get('/admin/thong-tin-khach-hang/khach-hang-tra-gop', [CustomerContactController::class, 'showCredit'])->name('admin.customer.credit');
        Route::get('/admin/thong-tin-khach-hang/khach-hang-tra-gop/xuat-file', [CustomerContactController::class, 'exportCredit'])->name('admin.export.credit');

        Route::post('/admin/user/create', [UserController::class, 'store'])->name('admin.user.store');
        Route::post('/admin/user/delete/{id}', [UserController::class, 'destroy'])->name('admin.user.destroy');
        Route::post('/admin/user/changePassword', [UserController::class, 'changePassword'])->name('admin.user.changePassword');

        Route::post('/iframe', [UserController::class, 'changePassword'])->name('iframe');
        Route::get('/admin/gallery', [PageController::class, 'changeGallery'])->name('admin.slider');
        Route::post('/admin/gallery/store', [PageController::class, 'storeGallery'])->name('admin.slider.store');
        Route::post('/admin/gallery/{id}', [PageController::class, 'destroyGallery'])->name('admin.slider.destroy');

        Route::post('/admin/icon/change', [PageController::class, 'changeIcon'])->name('admin.icon');
    }
});
