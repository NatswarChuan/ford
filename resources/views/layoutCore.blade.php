<!-- layout.blade.php -->
<!DOCTYPE html>
<html lang="en">

<head>
    @yield('head')

    @stack('css')
</head>

<body>
    @yield('body')

    @stack('js')
</body>

</html>
