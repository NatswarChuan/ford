@push('css')
    <style>
        .wrapper {
            position: fixed;
            bottom: 50px;
            right: var(--bs-gutter-x,.75rem);
        }
        .zalo{
            width: 60px;
            height: 60px;
            border-radius: 50%;
        }
        #scrollBtn {
            border: none;
            outline: none;
            color: white;
            cursor: pointer;
            font-size: 18px;
            background: #00000066;
            border-radius: 60px;
            bottom: 74px;
            box-shadow: rgba(0, 0, 0, 0.15) 0px 4px 12px 0px;
            display: none;
            height: 60px;
            width: 60px;
            z-index: 2147483644;
        }

        #scrollBtn:hover {
            background-color: #000000cc;
        }

        .btn-wrapper {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            margin-bottom: 5px;
            z-index: 2147483644;
        }
    .contact-name{
        max-width: 150px;
        font-size: 16px;
    }
    </style>
@endpush

<section class="footer">
    <div class="container-fluid" style="--bs-gutter-x: 6.5rem">
        <div class="row footer-content contact">
            <div class="col-md-6 col-company">
                <div class="footer-logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="Logo">
                        <div class="contact-name text-center text-white"></div>
                    </a>
                </div>
                <ul class="company">
                    <li class="company-info">
                        <ul>
                            <li class="name">
                                <span>ĐỊA CHỈ SHOWROOM</span>
                            </li>
                            <li class="lisence">
                                <span class="text-footer contact-address"></span>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="footer-menu">
                    <li class="menu-title">LIÊN HỆ VỚI CHÚNG TÔI</li>
                    <li class="footer-menu-item">
                        <a class="phone">
                            <i class="fa fa-phone"></i>
                        </a>
                    </li>
                    <li class="footer-menu-item">
                        <a class="email">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-6 col-product footer-desktop" id="menuDesktop">
            </div>

        </div>
    </div>
</section>

<div class="wrapper">
    <div class="btn-wrapper">
        <button onclick="topFunction()" id="scrollBtn" title="Scroll to Top"><i class=" fa fa-angle-up"
                aria-hidden="true" role="img"></i></button>
    </div>
    <div class="btn-wrapper">
        <a id="linkzalo" target="_blank">
            <div class="fcta-zalo-vi-tri-nut">
                <div id="fcta-zalo-tracking" class="fcta-zalo-nen-nut">
                    <div id="fcta-zalo-tracking" class="fcta-zalo-ben-trong-nut">
                        <img src="{{ asset('images/zalo.png') }}" alt="Số điện thoại" class="zalo">
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="btn-wrapper">
        <a class="phone-btn">
            <img src="{{ asset('images/phone.svg') }}" alt="Số điện thoại">
        </a>
    </div>
</div>





<!-- add plugin -->

<!-- end add plugin -->

@push('js')
    <script src="https://uhchat.net/code.php?f=58df49"></script>

    <script>
        const imgs = document.querySelectorAll('.ql-editor p img');
        imgs && imgs.length && imgs.forEach((item) => {
            item.removeAttribute("height");
            item.removeAttribute("width");
        });
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollBtn").style.display = "block";
            } else {
                document.getElementById("scrollBtn").style.display = "none";
            }
        }

        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
    <!-- Messenger Plugin chat Code -->
    <!--<div id="fb-root"></div>-->

    <!-- Your Plugin chat code -->
    <!--<div id="fb-customer-chat" class="fb-customerchat">-->
    <!--</div>-->

    <!--<script>
        -- >
        <
        !--
        var chatbox = document.getElementById('fb-customer-chat');
        -- >
        <
        !--chatbox.setAttribute("page_id", "103474265800974");
        -- >
        <
        !--chatbox.setAttribute("attribution", "biz_inbox");
        -- >
        <
        !--
    </script>-->

    <!-- Your SDK code -->
    <!--<script>
        -- >
        <
        !--window.fbAsyncInit = function() {
            -- >
            <
            !--FB.init({
                -- >
                <
                !--xfbml: true,
                -- >
                <
                !--version: 'v17.0'-- >
                    <
                    !--
            });
            -- >
            <
            !--
        };
        -- >

        <
        !--(function(d, s, id) {
            -- >
            <
            !--
            var js, fjs = d.getElementsByTagName(s)[0];
            -- >
            <
            !--
            if (d.getElementById(id)) return;
            -- >
            <
            !--js = d.createElement(s);
            js.id = id;
            -- >
            <
            !--js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
            -- >
            <
            !--fjs.parentNode.insertBefore(js, fjs);
            -- >
            <
            !--
        }(document, 'script', 'facebook-jssdk'));
        -- >
        <
        !--
    </script>-->
    <script src="{{ asset('js/footer.js') }}"></script>
@endpush
