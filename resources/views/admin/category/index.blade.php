@extends('/admin/layout')

@section('title', 'Các dòng sản phẩm')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @include('admin.modals.createCategoryModal')

    <div class="container-fluid">
        <div class="pb-3 d-lg-flex justify-content-lg-end">
            <button type="button" class="btn btn-primary m-0 font-weight-bold" data-toggle="modal" data-target="#addModal">
                Thêm dòng sản phẩm
            </button>
        </div>
        <div class="card shadow">
            <div class="card-body">
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Tên dòng sản phẩm</th>
                                <th>Trạng thái</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($categories as $category)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.product.category', $category->id) }}"> {{ $category->name }}
                                        </a>
                                    </td>
                                    <td>{{ $category->status ? 'Active' : 'Inactive' }}</td>
                                    <td style="display: flex; justify-content: space-between;">
                                        <a class="text-primary" data-toggle="modal"
                                            data-target="#editModal{{ $category->id }}"><i class="fas fa-edit"></i></a>
                                        <form id="deletePostForm{{ $category->id }}"
                                            action="{{ route('admin.category.destroy', $category->id) }}" method="POST"
                                            style="display: inline;">
                                            @csrf
                                            <a class="text-danger"
                                                onclick="document.getElementById('deletePostForm{{ $category->id }}').submit(); return false;"><i
                                                    class="fas fa-trash-alt"></i></a>
                                        </form>
                                    </td>
                                </tr>

                                <div class="modal fade" id="editModal{{ $category->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="editModalLabel{{ $category->id }}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editModalLabel{{ $category->id }}">Sửa Category
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form id="updatePostForm{{ $category->id }}"
                                                    action="{{ route('admin.category.update', $category->id) }}"
                                                    method="POST">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="name{{ $category->id }}">Tên:</label>
                                                        <input type="text" class="form-control"
                                                            id="name{{ $category->id }}" name="name"
                                                            value="{{ $category->name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="status{{ $category->id }}" name="status"
                                                                {{ $category->status ? 'checked' : '' }}>
                                                            <label class="custom-control-label"
                                                                for="status{{ $category->id }}">Trạng thái</label>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Đóng</button>
                                                <button type="button" class="btn btn-primary"
                                                    onclick="document.getElementById('updatePostForm{{ $category->id }}').submit();">Lưu</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Tên dòng sản phẩm</strong></td>
                                <td><strong>Trạng thái</strong></td>
                                <td><strong>Chức năng</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                    {{ $categories->links('/admin/category/pagination') }}

                </nav>
            </div>
        </div>
    </div>
@endsection('content')
