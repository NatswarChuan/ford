@extends('/admin/layout')

@section('title', 'Chỉnh sửa bài viết')

@section('content')

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif

    <div class="container-fluid">
        <div class="card-body">
            <form id="form" action="{{ route('admin.post.update', $post->slug) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div>Hình ảnh <button type="button" class="btn btn-primary" id="openModalButton">
                                    Chọn hình ảnh
                                </button></div>
                            <input type="text" id="image" name="image" value="{{ $post->image }}" hidden>
                            <img id="preview" src="{{ asset('/storage/' . $post->image) }}" alt="Hình ảnh trước"
                                style="max-width: 100%; margin-top: 10px;">
                        </div>
                        @include('admin.modals.inputImageModal')

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Tiêu đề</label>
                            <input type="text" class="form-control" id="title" name="title"
                                value="{{ $post->title }}">
                        </div>
                        <div class="form-group">
                            <label for="type">Loại bài viết</label>
                            <select class="form-control" id="type" name="type">
                                <option value="1" {{ $post->type == 1 ? 'selected' : '' }}>Bài viết</option>
                                <option value="2" {{ $post->type == 2 ? 'selected' : '' }}>Dịch vụ</option>
                                <option value="2" {{ $post->type == 3 ? 'selected' : '' }}>Trả góp</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="published_date">Ngày đăng</label>
                            <input type="date" class="form-control" id="published_date" name="published_date"
                                value="{{ $post->published_date->format('Y-m-d') }}">
                        </div>
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="status" name="status"
                                    {{ $post->status ? 'checked' : '' }}>
                                <label class="custom-control-label" for="status"></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Nội dung bài viết</label>
                    <div class="form-control" id="description">{!! $post->content !!}</div>
                    <textarea name="content" id="description-content" hidden></textarea>
                </div>

                <button type="submit" id="submit" class="btn btn-primary">Lưu</button>
            </form>
        </div>
    </div>
    @push('css')
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.0.0/quill.bubble.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
    @endPush
    @push('js')
        <script>
            document.getElementById('openModalButton').addEventListener('click', function() {
                flagAvatar = true;
                getImages();
                modal.show();

            });
            const form = document.getElementById('form');

            form.addEventListener('submit', function(event) {
                const img = document.getElementById('image');
                const preview = document.getElementById('preview');
                img.value = preview.src.replace("{{ asset('/storage/') }}",'');
                form.submit();
            })
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
        <script src="{{ asset('/js/quill.js') }}"></script>
    @endPush
@endSection
