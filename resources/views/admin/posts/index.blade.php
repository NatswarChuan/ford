@extends('/admin/layout')

@section('title', 'Tin tức và sự kiện')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="container-fluid">
        <div class="pb-3 d-lg-flex justify-content-lg-end">
            <a class="btn btn-primary m-0 font-weight-bold" href="{{ route('admin.post.create') }}">Thêm bài viết</a>
        </div>
        <div class="card shadow">
            <div class="card-body">
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Tiêu đề</th>
                                <th>Ngày đăng</th>
                                <th>Ngày tạo</th>
                                <th>Tác giả</th>
                                <th>Trạng thái</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($posts as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->published_date->format('d/m/Y') }}</td>
                                    <td>{{ $post->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $post->user->name }}</td>
                                    <td>{{ $post->status ? 'Active' : 'Inactive' }}</td>
                                    <td style="display: flex; justify-content: space-between;">
                                        <a href="{{ route('admin.post.edit', $post->slug) }}"><i
                                                class="fas fa-edit"></i></a>
                                        <form id="deletePostForm{{ $post->id }}"
                                            action="{{ route('admin.post.destroy', $post->slug) }}" method="POST"
                                            style="display: inline;">
                                            @csrf
                                            <a class="text-danger"
                                                onclick=" if(confirm('Xóa bài viết  {{ $post->title }}')) {
                                        document.getElementById('deletePostForm{{ $post->id }}').submit();
                                                            } return false;"><i
                                                    class="fas fa-trash-alt "></i></a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Tiêu đề</strong></td>
                                <td><strong>Ngày đăng</strong></td>
                                <td><strong>Ngày tạo</strong></td>
                                <td><strong>Tác giả</strong></td>
                                <td><strong>Trạng thái</strong></td>
                                <td><strong>Chức năng</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                    {{ $posts->links('/admin/posts/pagination') }}

                </nav>
            </div>
        </div>
    </div>
@endsection('content')
