@extends('/admin/layout')

@section('title', 'Bảng điều khiển')

@section('content')

    @include('admin.modals.createCategoryModal')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thông tin người dùng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('admin.user.store') }}">
                        @csrf
                        @if ($auth->role == App\Models\User::MASTER_USER)
                            <div class="form-group">
                                <label for="role">Quyền:</label>
                                <select class="form-control" id="role" name="role">
                                    <option value="admin">Admin</option>
                                    <option value="editor">Editor</option>
                                </select>
                            </div>
                        @else
                            <input type="text" class="form-control" id="role" name="role" value="editor" hidden>
                        @endif
                        <div class="form-group">
                            <label for="fullName">Họ và tên:</label>
                            <input type="text" class="form-control" id="fullName" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 p-5">
                <div class="card shadow border-left-primary py-2">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Bài
                                        viết</span>
                                </div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.post') }}"><span>&raquo; Danh sách bài viết</span></a>
                                </div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.post.create') }}"><span>&raquo; Thêm bài
                                            viết</span></a>
                                </div>
                            </div>
                            <div class="col-auto"><i class="fas fa-edit fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-5">
                <div class="card shadow border-left-success py-2">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-success font-weight-bold text-xs mb-1"><span>Dòng
                                        sản
                                        phẩm</span></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.category') }}"><span>&raquo; Danh sách các dòng sản
                                            phẩm</span></a></div>
                                <div>
                                    <a type="button" class="text-dark font-weight-bold h5 mb-0" data-toggle="modal"
                                        data-target="#addModal">
                                        <span>&raquo; Thêm dòng sản phẩm</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-auto"><i class="fas fa-car fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-5">
                <div class="card shadow border-left-info py-2">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-info font-weight-bold text-xs mb-1"><span>Sản
                                        phẩm</span>
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div><a class="text-dark font-weight-bold h5 mb-0"
                                                href="{{ route('admin.product') }}"><span>&raquo; Danh sách sản
                                                    phẩm</span></a></div>
                                        <div><a class="text-dark font-weight-bold h5 mb-0"
                                                href="{{ route('admin.product.create') }}"><span>&raquo; Thêm sản
                                                    sản
                                                    phẩm</span></a></div>
                                                    <div><a class="text-dark font-weight-bold h5 mb-0"
                                                href="{{ route('admin.product.oldCar') }}"><span>&raquo; Danh sách xe cũ</span></a></div>
                                        <div><a class="text-dark font-weight-bold h5 mb-0"
                                                href="{{ route('admin.product.oldCarCreate') }}"><span>&raquo; Thêm xe cũ</span></a></div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-auto"><i class="fas fa-car fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 p-5">
                <div class="card shadow border-left-warning py-2">
                    <div class="card-body">
                        <div class="row align-items-center no-gutters">
                            <div class="col mr-2">
                                <div class="text-uppercase text-warning font-weight-bold text-xs mb-1"><span>Thông
                                        tin
                                        khách
                                        hàng</span></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.customer') }}"><span>&raquo; Các khách hàng đã ghé
                                            qua</span></a></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.customer.test') }}"><span>&raquo; Khách hàng đăng ký
                                            lái
                                            thử</span></a></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.customer.price') }}"><span>&raquo; Khách hàng nhận
                                            báo
                                            giá</span></a></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.customer.ebrochure') }}"><span>&raquo; Khách hàng
                                            tải ebrochure</span></a></div>
                                <div><a class="text-dark font-weight-bold h5 mb-0"
                                        href="{{ route('admin.customer.credit') }}"><span>&raquo; Khách hàng trả góp</span></a></div>
                            </div>
                            <div class="col-auto"><i class="fas fa-comments fa-2x text-gray-300"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($auth->role != App\Models\User::EDIT_USER)
            <div class="row">
                <div class="col-6">
                    <div class="card shadow">
                        <div class="card-header pt-3">
                            <p class="text-primary m-0 font-weight-bold">Cài đặt thông tin</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" id="form" action="{{ route('admin.store') }}">
                                @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="name"><strong>Tên</strong></label><input
                                                class="form-control" type="text" id="name" placeholder=""
                                                name="name" value="{{ $store->name }}" /></div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="phone"><strong>Số điện
                                                    thoại</strong></label><input class="form-control" type="text"
                                                id="phone" placeholder="" name="phone"
                                                value="{{ $store->phone }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="email"><strong>Email</strong></label><input
                                                class="form-control" type="email" id="email" placeholder=""
                                                name="email" value="{{ $store->email }}" /></div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="address"><strong>Địa
                                                    chỉ</strong></label><input class="form-control" type="text"
                                                id="address" placeholder="" name="address"
                                                value="{{ $store->address }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="iframe"><strong>Định vị</strong></label>
                                    <input class="form-control" type="text" name="iframe"
                                        value="{{ $store->iframe }}" id="iframe">
                                </div>
<div class="form-group">
                    <label>Liên hệ</label>
                    <div class="form-control" id="contact">{!!  $store->contact !!}</div>
                    <textarea name="contact" id="contact-content" hidden></textarea>
                </div>
                <div class="form-group">
                    <label>Trả góp</label>
                    <div class="form-control" id="credit">{!!  $store->credit !!}</div>
                    <textarea name="credit" id="credit-content" hidden></textarea>
                </div>
                                <div class="form-group mb-0"><button class="btn btn-primary btn-sm"
                                        type="submit">Lưu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card shadow my-2">
                        <div class="d-flex card-body justify-content-between">
                            <form method="POST" action="{{ route('admin.icon') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="icon" id="inputIcon" />
                                <img src="{{ asset('images/logo.png') }}" width="100" id="iconImage" />
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div id="iframeShow">
                        {!! str_replace("<iframe class=\"w-100 h-100\" ", '<iframe ', $store->iframe) !!}
                    </div>

                </div>
            </div>

            <div class="card shadow">
                <div class="card-header py-3">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><span>Thêm
                            người dùng</span></button>
                </div>
                <div class="card-body">
                    <div class="table-responsive table" id="dataTable" role="grid" aria-describedby="dataTable_info">
                        <table class="table my-0" id="dataTable">
                            <thead>
                                <tr>
                                    <th>Họ và tên</th>
                                    <th>Email</th>
                                    <th>Quyền</th>
                                    <th>Chức năng</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id == $auth->id ? $user->name . ' (me)' : $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role }}</td>
                                        <td style="display: flex; justify-content: space-between;">
                                            @if (
                                                ($auth->role == App\Models\User::MASTER_USER ||
                                                    ($auth->role == App\Models\User::ADMIN_USER && $user->role == App\Models\User::EDIT_USER)) &&
                                                    $auth->id != $user->id)
                                                <form id="deletePostForm{{ $user->id }}"
                                                    action="{{ route('admin.user.destroy', $user->id) }}" method="POST"
                                                    style="display: inline;">
                                                    @csrf
                                                    <a class="text-danger"
                                                        onclick=" if(confirm('Xóa người dùng {{ $user->name }}')) {
                                        document.getElementById('deletePostForm{{ $user->id }}').submit();
                                                            } return false;"><i
                                                            class="fas fa-trash-alt "></i></a>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><strong>Họ và tên</strong></th>
                                    <th><strong>Email</strong></th>
                                    <th><strong>Quyền</strong></th>
                                    <th><strong>Chức năng</strong></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @push('js')
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
        <script>
        let toolbar= {
    theme: 'snow',
    modules: {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }],
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }],
            [{
                'direction': 'rtl'
            }],
            [{
                'align': []
            }],
            ['link', 'video'],
            ['clean']
        ]
    },
    placeholder: 'Nhập nội dung ở đây...'
}
        var quillcontact = new Quill('#contact', toolbar);
        var quillcredit = new Quill('#credit', toolbar);



document.querySelector('#form').addEventListener('submit', function (e) {
    const contact = document.querySelector('#contact .ql-editor').innerHTML;
    const contactcontent = document.querySelector('#contact-content');
    contactcontent.value = '<div class="ql-snow"><div class="ql-editor">'+contact+'</div></div>';
    
     const credit = document.querySelector('#credit .ql-editor').innerHTML;
    const creditcontent = document.querySelector('#credit-content');
    creditcontent.value = '<div class="ql-snow"><div class="ql-editor">'+credit+'</div></div>';
    
});
            document.getElementById('inputIcon').addEventListener('change', function(e) {
                const file = e.target.files[0];
                const reader = new FileReader();
                reader.onload = function(event) {
                    const imageUrl = event.target.result;
                    const iconImage = document.getElementById('iconImage');
                    iconImage.src = imageUrl;
                };
                reader.readAsDataURL(file);
            })
        </script>
      
    @endpush
        @push('css')
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.0.0/quill.bubble.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
    @endPush
@endsection
