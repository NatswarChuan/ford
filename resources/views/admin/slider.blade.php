@extends('/admin/layout')

@section('title', 'Hình ảnh')

@section('content')

    @include('admin.modals.createCategoryModal')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    @include('admin.modals.inputImageModal')

    <div class="container-fluid">
        <div class="pb-3 d-lg-flex justify-content-lg-end">
            <button class="btn btn-primary m-0 font-weight-bold" id="addImageGallery">Thêm hình ảnh</button>
        </div>
        <div class="card shadow">
            <div class="card-body">
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Hình ảnh</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sliders as $slider)
                                <tr>
                                    <td><img class="mr-2" src="{{ asset('/storage/' . $slider->image) }}"
                                            width="500" />
                                    </td>
                                    <td>
                                        <form id="deletePostForm{{ $slider->id }}"
                                            action="{{ route('admin.slider.destroy', $slider->id) }}" method="POST"
                                            style="display: inline;">
                                            @csrf
                                            <a class="text-danger"
                                                onclick=" if(confirm('Xóa hình ảnh')) {
                                        document.getElementById('deletePostForm{{ $slider->id }}').submit();
                                                            } return false;"><i
                                                    class="fas fa-trash-alt "></i></a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Hình ảnh</strong></td>
                                <td><strong>Chức năng</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="formInsertImage" method="post" action="{{ route('admin.slider.store') }}">@csrf</form>
    @push('js')
        <script>
            const addImageGallery = document.getElementById('addImageGallery');
            addImageGallery.addEventListener('click', function(e) {
                getImages();
                modal.show();
            })

            function chosenImage(imageUrl) {
                console.log(imageUrl);
                const formInsertImage = document.getElementById('formInsertImage');
                formInsertImage.innerHTML += `<input name="image" value="${imageUrl.replace("/storage/", "")}" hidden>`;
                formInsertImage.submit();
            }
        </script>
    @endPush
@endsection
