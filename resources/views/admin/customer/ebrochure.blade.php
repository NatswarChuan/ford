@extends('/admin/layout')

@section('title', 'Thông tin khách hàng tải ebrochure')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    <div class="container-fluid">
        <div class="pb-3 d-lg-flex justify-content-lg-between">
            <div>
                <a class="btn btn-primary m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.customer.credit') }}">Thông
                    tin khách hàng đăng ký trả góp</a>
                <a class="btn btn-success m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.customer') }}">Thông tin
                    khách hàng</a>
                <a class="btn btn-info m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.customer.test') }}">Thông
                    tin khách hàng đăng ký lái thử</a>
                <a class="btn btn-warning m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.customer.price') }}">Thông tin khách hàng đăng ký nhận báo giá </a>
            </div>
            <a class="btn btn-primary m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.export.ebrochure') }}"
                target="_blank">Xuất
                file excel</a>
        </div>
        <div class="card shadow">
            <div class="card-body">
                <div class="d-flex mb-3">
                    <form class="form-inline my-2 my-lg-0 w-100 justify-content-between">
                        @csrf
                        <div class="form-group px-1">
                            <label for="startDate">Ngày bắt đầu:</label>
                            <input type="date" class="form-control" id="startDate" name="start_date">
                        </div>
                        <div class="form-group px-1">
                            <label for="endDate">Ngày kết thúc:</label>
                            <input type="date" class="form-control" id="endDate" name="end_date">
                        </div>

                        <div class="d-lg-flex justify-content-lg-end">
                            <button type="submit" class="btn btn-primary mx-3">Lọc</button>
                        </div>
                    </form>

                </div>
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Nội dung</th>
                                <th>Sản phẩm</th>
                                <th>Ngày đăng ký lái thử</th>
                                <th>Ngày tạo</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($customers as $customer)
                                <tr>
                                    <td>{{ $customer->full_name }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ $customer->phone_number }}</td>
                                    <td>{{ $customer->address }} </td>
                                    <td>{{ $customer->description }}</td>
                                    <td>{{ isset($customer->product->name) ? $customer->product->name : 'Sản phẩm đã không còn' }}</td>
                                    <td>{{ \Carbon\Carbon::parse($customer->time)->format('d/m/Y') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($customer->created_at)->format('d/m/Y') }}</td>
                                    <td> <form id="deleteContactForm{{ $customer->id }}"
                                            action="{{ route('admin.customer.destroy', ['id' => $customer->id]) }}"
                                            method="POST" style="display: inline;">
                                            @csrf
                                            <a class="text-danger"
                                                onclick=" document.getElementById('deleteContactForm{{ $customer->id }}').submit();"><i
                                                    class="fas fa-trash-alt "></i></a>
                                        </form></td>
                                </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Họ và tên</strong></td>
                                <td><strong>Email</strong></td>
                                <td><strong>Số điện thoại</strong></td>
                                <td><strong>Địa chỉ</strong></td>
                                <td><strong>Nội dung</strong></td>
                                <th><strong>Ngày đăng ký lái thử</strong></th>
                                <td><strong>Ngày tạo</strong></td>
                                <td><strong>Xóa</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                    {{ $customers->links('/admin/customer/pagination') }}
                </nav>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            const currentURL = window.location.href;
            const url = new URL(currentURL);
            const searchParams = new URLSearchParams(url.search);
            const startDate = searchParams.get('start_date');
            const endDate = searchParams.get('end_date');
            const exportExcel = document.getElementById('exportExcel');
            const href = "{{ route('admin.export.ebrochure') }}" + `?start_date=${startDate}&end_date=${endDate}`;
            if (startDate && endDate) {
                exportExcel.href = href;
            }
        </script>
    @endPush
@endsection('content')
