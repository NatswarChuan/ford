<div class="modal fade" id="imageModal" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageModalLabel">Danh sách hình ảnh</h5>
                <div class="d-flex flex-row-reverse">
                    <button id="addImage" type="button" class="btn btn-primary">Thêm hình ảnh</button>
                    <input id="imageInput" type="file" hidden />
                </div>
            </div>
            <div class="modal-body">
                <div class="image-container" style="column-count: 3;" id="imageList">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="optionModal" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Chọn hoặc xóa hình ảnh?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Chọn ảnh</button>
                <button type="button" class="btn btn-danger">Xóa ảnh</button>
            </div>
        </div>
    </div>
</div>


@push('js')
    <script src="{{ asset('/js/imageModal.js') }}"></script>
@endPush
