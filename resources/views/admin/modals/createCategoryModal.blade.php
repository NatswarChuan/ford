 <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="addModalLabel">Thêm Category</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 <form id="addPostForm" action="{{ route('admin.category.create') }}" method="POST">
                     @csrf
                     <div class="form-group">
                         <label for="name">Tên:</label>
                         <input type="text" class="form-control" id="name" name="name" value="">
                     </div>
                     <div class="form-group">
                         <div class="custom-control custom-switch">
                             <input type="checkbox" class="custom-control-input" id="status" name="status" checked>
                             <label class="custom-control-label" for="status">Trạng thái</label>
                         </div>
                     </div>
                 </form>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                 <button type="button" class="btn btn-primary"
                     onclick="document.getElementById('addPostForm').submit();">Lưu</button>
             </div>
         </div>
     </div>
 </div>
