@extends('/admin/layout')

@section('title', 'Sản phẩm')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    <div class="container-fluid">
        <div class="pb-3 d-lg-flex justify-content-lg-between">
            <div>
                <a class="btn btn-success m-0 font-weight-bold" id="exportExcel" href="{{ route('admin.product.oldCar') }}">Danh sách xe cũ</a>
            </div>
            <a class="btn btn-primary m-0 font-weight-bold" href="{{ route('admin.product.create') }}">Thêm sản
                phẩm</a>
        </div>
        <div class="card shadow">
            <div class="card-body">
                <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                    <table class="table my-0" id="dataTable">
                        <thead>
                            <tr>
                                <th>Tên sản phẩm</th>
                                <th>Phiên bản</th>
                                <th>Giá</th>
                                <th>Dòng sản phẩm</th>
                                <th>Trạng thái</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->version }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td><a href="{{ route('admin.product.category', $product->category->id) }}">
                                            {{ $product->category->name }}
                                        </a> </td>
                                    <td>{{ $product->status ? 'Active' : 'Inactive' }}</td>
                                    <td style="display: flex; justify-content: space-between;">
                                        <a href="{{ route('admin.product.edit', ['slug' => $product->slug]) }}"><i
                                                class="fas fa-edit"></i></a>
                                        <form id="deletePostForm{{ $product->id }}"
                                            action="{{ route('admin.product.destroy', ['slug' => $product->slug]) }}"
                                            method="POST" style="display: inline;">
                                            @csrf
                                            <a class="text-danger"
                                                onclick="   if(confirm('Xóa sản phẩm  {{ $product->name }}')) {
	                                                            document.getElementById('deletePostForm{{ $product->id }}').submit();
                                                            } return false;"><i
                                                    class="fas fa-trash-alt "></i></a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Tiêu đề</strong></td>
                                <td><strong>Ngày đăng</strong></td>
                                <td><strong>Ngày tạo</strong></td>
                                <td><strong>Tác giả</strong></td>
                                <td><strong>Trạng thái</strong></td>
                                <td><strong>Chức năng</strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                    {{ $products->links('/admin/products/pagination') }}

                </nav>
            </div>
        </div>
    </div>
@endsection('content')
