@extends('/admin/layout')

@section('title', 'Sản phẩm')

@push('css')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.0.0/quill.bubble.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.2/awesomplete.min.css" />
@endPush

@section('content')

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    @include('admin/modals/colorModal')
    @include('admin.modals.inputImageModal')

    <style>
        input[type=color] {
            border-radius: 50%;
        }

        input[type=color]::-webkit-color-swatch {
            border: none;
            border-radius: 50%;
            padding: 0;
        }

        input[type=color]::-webkit-color-swatch-wrapper {
            border: none;
            border-radius: 50%;
            padding: 0;
        }
    </style>

    <div class="container-fluid">
        <div class="card-body">
            <form id="form" action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type">Dòng sản phẩm</label>
                            <select class="form-control" id="type" name="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="version">Phiên bản</label>
                            <input type="text" class="form-control" id="version" name="version">
                        </div>
                        <div class="form-group">
                            <label for="name">Tên sản phẩm</label>
                            <input type="text" class="form-control" id="name" disabled>
                            <input type="text" class="form-control" id="name_send" name="name" hidden>
                        </div>
                        <div class="form-group">
                            <label for="price">Giá bản</label>
                            <input class="form-control" id="price" name="price">
                        </div>

                        <div class="form-group">
                            <embed id="pdfViewer" src="" type="application/pdf" width="100%" height="600px"
                                hidden />

                            <div class="form-group">
                                <label for="pdfFile">Ebrochure</label>
                                <input type="file" id="pdfFile" name="ebrochure" class="form-control" accept=".pdf">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="status" name="status">
                                <label class="custom-control-label" for="status"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Màu sắc</label>
                            <button type="button" class="btn btn-success" id="insertColor">
                                Thêm màu
                            </button>
                        </div>
                        <div class="form-group" id="colorGroupInput">

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Nội dung bài viết</label>
                    <div class="form-control" id="description"></div>
                    <textarea name="content" id="description-content" hidden></textarea>
                </div>

                <div class="form-group" id="tableGroupInput">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Tên thông số</label>
                        </div>
                        <div class="col-md-9">
                            <label>Thông số</label>
                        </div>
                    </div>
                    <div class="row pb-3">
                    </div>
                </div>
                <button type="button" class="btn btn-success" id="insertTable">
                    Thêm thông số
                </button>
                <button type="submit" id="submit" class="btn btn-primary">Lưu</button>
            </form>
        </div>
    </div>
@endSection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autocomplete.js/0.38.0/autocomplete.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.2/awesomplete.min.js"></script>

    <script>
        const fileInput = document.getElementById('pdfFile');
        const pdfViewer = document.getElementById('pdfViewer');
        const insertColorBtn = document.getElementById('insertColor');
        const insertTableBtn = document.getElementById('insertTable');
        const tableGroupInput = document.getElementById('tableGroupInput');
        const colorGroupInput = document.getElementById('colorGroupInput');
        const categoryInput = document.getElementById('type');
        const versionInput = document.getElementById('version');
        const nameInput = document.getElementById('name');
        const nameSendInput = document.getElementById('name_send');

        categoryInput.addEventListener('change', function() {
            nameInput.value = categoryInput.options[categoryInput.selectedIndex].innerHTML.trim() + " " +
                versionInput.value;
            nameSendInput.value = categoryInput.options[categoryInput.selectedIndex].innerHTML.trim() + " " +
                versionInput.value;
        })
        versionInput.addEventListener('input', function() {
            nameInput.value = categoryInput.options[categoryInput.selectedIndex].innerHTML.trim() + " " +
                versionInput.value;
            nameSendInput.value = categoryInput.options[categoryInput.selectedIndex].innerHTML.trim() + " " +
                versionInput.value;
        })
        
        fileInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            const fileURL = URL.createObjectURL(file);

            pdfViewer.setAttribute('src', fileURL);
            pdfViewer.hidden = false;
        });
        fileInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            const fileURL = URL.createObjectURL(file);

            pdfViewer.setAttribute('src', fileURL);
        });

        let i = 0;
        let j = 0;
        const suggestions = {{ Js::from($tables) }}

        function showModal(e) {
            getImages();
            modal.show();
        }
        insertColorBtn.addEventListener('click', showModal);
        insertTableBtn.addEventListener('click', function() {
            const tableInput = document.createElement('div');
            tableInput.classList.add('row');
            tableInput.classList.add('pb-3');
            tableInput.innerHTML = `
                <div class="col-md-3">
                    <input type="text" name="tables[${j}][title]" class="form-control myInput" placeholder="Tên thông số" />
                    <ul class="autocompleteList"></ul>
                 </div>
                 <div class="col-md-8">
                     <input type="text" name="tables[${j++}][content]" class="form-control" placeholder="Thông số" />
                 </div>
                 <div class="col-md-1">
                     <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt "></i></button>
                 </div>
                `;

            tableGroupInput.appendChild(tableInput);
            new Awesomplete(tableInput.querySelector('.myInput'), {
                minChars: 1,
                maxItems: 5,
                list: suggestions
            });
        });

        function chosenImage(imageUrl) {
            const imgInput = document.createElement('div');
            imgInput.classList.add('py-1');
            imgInput.classList.add('d-flex');
            imgInput.classList.add('align-items-center');
            imgInput.classList.add('justify-content-around');
            imgInput.innerHTML = `
                <img class="w-50" src="${imageUrl}" />
                <input type="text" name="colors[${++i}][image]" value="${imageUrl.replace('/storage/', '')}" hidden>
                 <div> 
                    <input type="text" name="colors[${i}][name]"/>
                </div>
                <div class="w-25 text-center">
                    <input style="width:40px;height:40px;" type="color" name="colors[${i}][color_code]">
                </div>
                <div>
                    <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt "></i></button>
                </div>`;
            colorGroupInput.appendChild(imgInput);
            imgInput.querySelector('button').addEventListener('click', function(e) {
                imgInput.parentNode.removeChild(imgInput);
            });
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <script src="{{ asset('/js/quill.js') }}"></script>
@endPush
