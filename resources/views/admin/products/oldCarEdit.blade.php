@extends('/admin/layout')

@section('title', 'Xe cũ')

@section('content')

    @if (session('fail'))
        <div class="alert alert-danger">
            {{ session('fail') }}
        </div>
    @endif
    @if (session('errors'))
        @foreach (session('errors') as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    @include('admin/modals/colorModal')
    @include('admin.modals.inputImageModal')

    <style>
        input[type=color] {
            border-radius: 50%;
        }

        input[type=color]::-webkit-color-swatch {
            border: none;
            border-radius: 50%;
            padding: 0;
        }

        input[type=color]::-webkit-color-swatch-wrapper {
            border: none;
            border-radius: 50%;
            padding: 0;
        }
    </style>

    <div class="container-fluid">
        <div class="card-body">
            <form id="form" action="{{ route('admin.product.oldCarUpdate', ['slug' => $product->slug]) }}" method="post"
                enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                         <div class="form-group">
                            <label for="name">Tên sản phẩm</label>
                            <input type="text" class="form-control" id="name" value="{{ $product->name }}" name="name">
                        </div>
                        <div class="form-group">
                            <label for="version">Phiên bản</label>
                            <input type="text" class="form-control" id="version" name="version"
                                value="{{ $product->version }}">
                        </div>
                       
                        <div class="form-group">
                            <label for="price">Giá bản</label>
                            <input class="form-control" id="price" name="price"
                                value="{{ $product->price }}">
                        </div>

                        <div class="form-group">
                            <label for="status">Trạng thái</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="status" name="status"
                                    {{ $product->status ? 'checked' : '' }}>
                                <label class="custom-control-label" for="status"></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Hình ảnh</label>
                            <button type="button" class="btn btn-success" id="insertColor">Thêm hình ảnh</button>
                        </div>
                        <div class="form-group" id="colorGroupInput">
                            @foreach ($product->colors as $color)
                                <div class="py-1 d-flex align-items-center justify-content-around">
                                    <img class="w-50" src="{{ asset('/storage/' . $color->image) }}">
                                    <input type="text" name="colors[{{ $loop->index }}][image]"
                                        value="{{ $color->image }}" hidden>
                                    <div><input type="text" name="colors[{{ $loop->index }}][name]" value="{{$color->name}}" hidden/></div>
                                    <div class="w-25 text-center">
                                        <input style="width:40px;height:40px;"
                                            name="colors[{{ $loop->index }}][color_code]"
                                            value="{{ $color->color_code }}" hidden>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-danger"
                                            onclick="actionRemoveImgInput(this.parentNode.parentNode)"><i
                                                class="fas fa-trash-alt "></i></button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Nội dung bài viết</label>
                    <div class="form-control" id="description">{!! $product->content !!}</div>
                    <textarea name="content" id="description-content" hidden>{{ $product->content }}</textarea>
                </div>

                <button type="submit" id="submit" class="btn btn-primary">Lưu</button>
            </form>

        </div>
    </div>
    @push('css')
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.0.0/quill.bubble.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.2/awesomplete.min.css" />
        <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
    @endPush
    @push('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.2/awesomplete.min.js"></script>
        <script>
            const insertColorBtn = document.getElementById('insertColor');
            const insertTableBtn = document.getElementById('insertTable');
            const colorGroupInput = document.getElementById('colorGroupInput');

            let i = {{count($product->colors)}};

            function showModal(e) {
                getImages();
                modal.show();
            }
            insertColorBtn.addEventListener('click', showModal);

            function actionRemoveTableInput(tableInput) {
                tableInput.parentNode.removeChild(tableInput);
            }

            function chosenImage(imageUrl) {
                const imgInput = document.createElement('div');
                imgInput.classList.add('py-1');
                imgInput.classList.add('d-flex');
                imgInput.classList.add('align-items-center');
                imgInput.classList.add('justify-content-around');
                imgInput.innerHTML = `
                <img class="w-50" src="${imageUrl}" />
                <input type="text" name="colors[${++i}][image]" value="${imageUrl.replace('/storage/', '')}" hidden>
                <div>
                    <input type="text" name="colors[${i}][name]" value="empty" hidden/>
                </div>
                <div class="w-25 text-center">
                    <input style="width:40px;height:40px;"  name="colors[${i}][color_code]" value="#000000" hidden>
                </div>
                <div>
                    <button type="button" class="btn btn-danger"><i class="fas fa-trash-alt "></i></button>
                </div>`;
                colorGroupInput.appendChild(imgInput);
                imgInput.querySelector('button').addEventListener('click', (e) => {
                    actionRemoveImgInput(imgInput)
                });
            }

            function actionRemoveImgInput(imgInput) {
                imgInput.parentNode.removeChild(imgInput);
            }
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
        <script src="{{ asset('/js/quill.js') }}"></script>
    @endPush
@endSection
