<nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
    <div class="container-fluid d-flex flex-column p-0">
        <hr class="sidebar-divider my-0">
        <ul class="navbar-nav text-light" id="accordionSidebar">
            <li class="nav-item"><a class="nav-link  {{ Request::routeIs('admin') ? 'active' : '' }}" href="{{ route('admin') }}"><i class="fas fa-tachometer-alt"></i><span>Bảng điều khiển</span></a></li>
            <li class="nav-item"><a class="nav-link  {{ Request::routeIs('admin.post*') ? 'active' : '' }}" href="{{ route('admin.post') }}"><i class="fas fa-edit"></i><span>Bài viết</span></a></li>
            <li class="nav-item"><a class="nav-link  {{ Request::routeIs('admin.category*') ? 'active' : '' }}" href="{{ route('admin.category') }}"><i class="fas fa-car"></i><span>Dòng sản phẩm</span></a></li>
            <li class="nav-item"><a class="nav-link  {{ Request::routeIs('admin.product*') ? 'active' : '' }}" href="{{ route('admin.product') }}"><i class="fas fa-car"></i><span>Sản phẩm</span></a></li>
            <li class="nav-item"><a class="nav-link {{ Request::routeIs('admin.customer*') ? 'active' : '' }}" href="{{ route('admin.customer') }}"><i class="fas fa-comments"></i><span>Thông tin khách hàng</span></a></li>
            <li class="nav-item"><a class="nav-link {{ Request::routeIs('admin.slider*') ? 'active' : '' }}" href="{{ route('admin.slider') }}"><i class="fas fa-image"></i><span>Hình ảnh</span></a></li>
             <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#changePasswordModal"><i class="fas fa-key"></i><span>Đổi mật khẩu</span></a>
            </li>
            <li class="nav-item">
                <form id="logoutForm" action="{{ route('logout') }}" method="post" style="display: none;">@csrf</form>
                <a id="logout" class="nav-link" onclick="document.getElementById('logoutForm').submit(); return false;"><i class="fas fa-sign-out-alt"></i>
                    <span>Đăng xuất</span>
                </a>
            </li>
        </ul>
        <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
    </div>
</nav>
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="changePasswordModalLabel">Đổi mật khẩu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.user.changePassword') }}">
                    @csrf
                    <div class="form-group">
                        <label for="currentPassword">Mật khẩu hiện tại:</label>
                        <input type="password" class="form-control" id="currentPassword" name="current_password">
                    </div>
                    <div class="form-group">
                        <label for="newPassword">Mật khẩu mới:</label>
                        <input type="password" class="form-control" id="newPassword" name="new_password">
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword">Xác nhận mật khẩu mới:</label>
                        <input type="password" class="form-control" id="confirmPassword" name="confirm_password">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
