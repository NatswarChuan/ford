<!DOCTYPE html>
<html>
<head>
    <title>{{$title[$contact->type]}}</title>
</head>
<body>
    <H1>{{$title[$contact->type]}}</H1>
    <p>Họ và tên: {{$contact->full_name}}</p>
    <p>Số điện thoại: {{$contact->phone_number}}</p>
    @isset($contact->address)
    <p>Địa chỉ: {{$contact->address}}</p>
    @endisset
    @isset($contact->product)
    <p>Sản phẩm: {{$contact->product->name}}</p>
    @endisset
    @isset($contact->time)
    <p>Thời gian: {{ \Carbon\Carbon::parse($contact->time)->format('d/m/Y') }}</p>
    @endisset
</body>
</html>
