<div class="pagination-main-new">
    @if ($paginator->hasPages())
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled page-item" aria-disabled="true" aria-label="« Previous">
                    <span aria-hidden="true"><i class="fas fa-arrow-left"></i></span>
                </li>
            @else
                <li class="prev">
                    <a class="page-link previ" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                        aria-label="« Previous"><i class="fas fa-arrow-left"></i></a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                    <li class="disabled page-item mb-1" aria-disabled="true"><span
                            class="bg-white text-dark border-0 page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active "><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item">
                                <a class="page-link" data-page="2" href="{{ $url }}">
                                    {{ $page }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a class="page-link previ" href="{{ $paginator->nextPageUrl() }}" rel="next"
                        aria-label="Next »"> <i class="fas fa-arrow-right"> </i></a>
                </li>
            @else
                <li class="disabled last" aria-disabled="true" aria-label="Next »">
                    <span aria-hidden="true"> <i class="fas fa-arrow-right"> </i></span>
                </li>
            @endif
        </ul>
    @endif
</div>
