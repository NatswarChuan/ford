@extends('layout')

@section('title', 'Liên hệ')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/box.css') }}">
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/checkbox-custom.css') }}">
    <style>
        .content-contact {
            margin-top: 0 !important;
        }
        .contact-store{
            max-width:50%!important;
            margin:0 auto;
        }
        @media only screen and (max-width: 991px){
    .contact-store{
        max-width:100%!important;
    }
}
    </style>
        <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
@endpush

@push('js')
    <script>
        const baseURL = "{{ asset('') }}";
    </script>
    <script src="{{ asset('js/contact.js') }}"></script>
@endpush

@section('content')
    <section class="contact-wrapper padding-top-with-menu-sticky">
        <div class="__contact_content">
            <div class="_box_contact">
                <div class="container">
                    <div style="width: 100%;
    margin-bottom: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2px #000000 solid;">
    <span style="padding: 1rem;
    font-size: 35px;
    font-weight: bold;">Kết nối với chúng tôi</span>
</div>
<div class="contact-store">{!! $store->contact !!}</div>
                    <ul class="box-contact">

                        <li class="box-item">
                            <ul class="box-content">
                                <li class="icon">
                                    <img class="img-fluid" src="https://kiagovap.vn/themes/main/images/contact/phone.jpg"
                                        alt="Icon">
                                </li>

                                <li class="content">
                                    <a class=" VN-KiaSignature-Bold" href="tel: 0938 809 860">Hotline:
                                        {{ $store['phone'] }}</a><br>
                                </li>
                            </ul>
                        </li>
                        <li class="box-item">
                            <ul class="box-content">
                                <li class="icon">
                                    <img class="img-fluid" src="https://kiagovap.vn/themes/main/images/contact/email.jpg"
                                        alt="Icon">
                                </li>


                                <li class="content">
                                    <a class="VN-KiaSignature-Bold"
                                        href="mailto:{{ $store['email'] }}">{{ $store['email'] }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <section class="_form_contact">
                <div class="container">
                    <div class="___form ct_form">
                        <h3 class="form-title c-text-base">VUI LÒNG ĐỂ LẠI THÔNG TIN LIÊN HỆ THEO MẪU BÊN DƯỚI</h3>
                        <div class="_form_content">
                            <form id="form-contact" class="form-submit-redirect" autocomplete="off" method="post">
                                <input type="hidden" name="_token" value="T7zlUhyzTqEixtOf4qJTxcBHr7S0PEBGwarbUbei">
                                <div class="form-group group-input-custom">
                                    <div class="label">
                                        <span>Họ/Tên <strong class="text-danger">*</strong></span>
                                    </div>
                                    <div class="input-custom">
                                        <div class="input">
                                            <input autocomplete="off" name="last_name" placeholder="Họ" value=""
                                                type="text" required>
                                            <span class="error-msg"></span>
                                        </div>
                                        <div class="input">
                                            <input autocomplete="off" name="first_name" type="text" value=""
                                                placeholder="Tên" required>
                                            <span class="error-msg"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group group-input-custom">
                                    <div class="label">
                                       <span>Số điện thoại <strong class="text-danger">*</strong></span>
                                    </div>
                                    <div class="input-custom">
                                        <div class="input full-width">
                                            <input autocomplete="off" name="phone" type="tel"
                                                placeholder="Số điện thoại" value="" pattern="[0-9]{10,11}">
                                            <span class="error-msg"></span>
                                            <span class="error-msg"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group group-input-custom">
                                    <div class="label">
                                        <span>Địa chỉ <strong class="text-danger">*</strong></span>
                                    </div>
                                    <div class="input-custom">
                                        <div class="input full-width">
                                            <input autocomplete="off" name="address" type="text" placeholder="Địa chỉ"
                                                value="" required>
                                            <span class="error-msg"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group group-input-custom">
                                    <div class="label">
                                        <span class="content-contact">Nội dung <strong class="text-danger"></strong></span>
                                    </div>
                                    <div class="input-custom"><grammarly-extension data-grammarly-shadow-root="true" style="position: absolute; top: 0px; left: 0px; pointer-events: none;" class="dnXmp"></grammarly-extension><grammarly-extension data-grammarly-shadow-root="true" style="position: absolute; top: 0px; left: 0px; pointer-events: none;" class="dnXmp"></grammarly-extension>
                                        <div class="textarea">
                                            <textarea autocomplete="off" name="contact_body" rows="5" spellcheck="false"></textarea>
                                            <span class="error-msg"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-action">
                                    <button id="submitDrive" class="black-button submit" type="submit">
                                        Gửi
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="toast align-items-center text-white bg-danger border-0" role="alert"
                        aria-live="assertive" aria-atomic="true">
                        <div class="d-flex">
                            <div class="toast-body">
                            </div>
                            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                                aria-label="Close"></button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection
