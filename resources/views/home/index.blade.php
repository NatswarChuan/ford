@extends('layout')

@section('title', $store->name)

@push('css')
    <link rel="stylesheet" href="{{ asset('css/select-car.css') }}">
    <style>
        .content {
            height: 150px;
            position: absolute;
            bottom: 5%;
            left: 50%;
            color: #fff;
            z-index: 2;
            width: 85%;
            transform: translate(-50%);
        }

        @media (max-width: 767px) {
            .mobile-scroll-container {
                overflow-x: scroll;
                white-space: nowrap;
            }

            .col-4 {
                width: 75%;
                display: inline-block;
            }
        }
    </style>
@endpush

@section('content')
    @include('home.slide', ['sliders' => $sliders])
    <div class="text-center text-white text-uppercase w-100" style="
    margin-top: 2rem;
    background: linear-gradient(112.1deg, rgb(63, 76, 119) 11.4%, rgb(32, 38, 57) 70.2%);">
   <h1 class="p-3">dòng xe đang phân phối</h1>
</div>

    @include('home.home-product', ['categories' => $categories])

    <section>
        <div class="mt-5 main container">
            <div class="justify-content-between">
                <div class="mobile-scroll-container">
                    <div class="row">
                        <div class="col-4 position-relative">
                            <a href="{{ route('product.index') }}" style="display: flex;">
                                <div class="content">
                                    <h3>Chọn xe và giá</h3>
                                    <div class="bottom">
                                        <div class="button-click-wrapper">
                                            <div class="button-click">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-hover">
                                    <a href="{{ route('product.index') }}">
                                        <img src="{{ asset('img/san-pham.jpg') }}" alt="" class="w-100"></a>
                                </div>
                            </a>
                        </div>
                        <div class="col-4 position-relative">
                            <a href="{{ route('product.ebrochure') }}" style="display: flex;">
                                <div class="content">
                                    <h3>Tải brochure</h3>
                                    <div class="bottom">
                                        <div class="button-click-wrapper">
                                            <div class="button-click">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-hover">
                                    <a href="{{ route('product.ebrochure') }}">
                                        <img src="{{ asset('img/tai.jpg') }}" alt="" class="w-100"></a>

                                </div>
                            </a>
                        </div>
                        <div class="col-4 position-relative">
                            <a href="{{ route('product.credit') }}" style="display: flex;">
                                <div class="content">
                                    <h3>Mua Xe Trả góp</h3>
                                    <div class="bottom">
                                        <div class="button-click-wrapper">
                                            <div class="button-click">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-hover">
                                    <a href="{{ route('product.credit') }}">
                                        <img src="{{ asset('img/tin-tuc.jpg') }}" alt="" class="w-100"></a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('home.home-post', ['postsHot' => $postsHot])

@endsection

@push('js')
    <script>
        const baseURL = "{{ asset('') }}";
   var width = window.innerWidth;
   if(width < 767){
       document.querySelector('.mobile-scroll-container .row').classList.remove('row');
   }
    </script>
@endpush
