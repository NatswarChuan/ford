@push('css')
    <link rel="stylesheet" href="{{ asset('css/slide.css') }}">
@endpush

<div class="slideshow-container">
    @foreach ($sliders as $item)
        <div class="mySlides">
            <img src="{{ asset('/storage/' . $item['image']) }}" style="width:100%">
        </div>
    @endforeach
</div>

@push('js')
    <script src="{{ asset('js/slide.js') }}"></script>
@endpush
