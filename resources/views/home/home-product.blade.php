@push('css')
    <link rel="stylesheet" href="{{ asset('css/navTab.css') }}">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select-color.css') }}">
@endpush

@push('js')
    <script src="{{ asset('js/select-color.js') }}"></script>
    <script>
        getProduct("{{ $slug }}");
    </script>
@endpush

<section class="home-product-wrapper product-wrapper mt-5"
    style="background-image: url('{{ asset('img/354796574_1017253975946545_2646442793656530249_n.png') }}');">
    <div class="container">
        <div class="product-menu">
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                @foreach ($categories as $item)
                    <div class="nav-item nav-link c-text-lg" id="hatchback-tab" data-toggle="tab" href="#hatchback">
                        <span id="dropdownMenu{{ $item['id'] }}" data-bs-toggle="dropdown"
                            aria-expanded="false">{{ $item['name'] }}</span>
                        <ul class="dropdown-menu" style="width: 100%!important;" aria-labelledby="dropdownMenu{{ $item['id'] }}">
                            @foreach ($item['products'] as $product)
                                <li><a class="dropdown-item get-product" style="white-space: normal;"
                                        data-slug="{{ $product['slug'] }}">{{ $product['name'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach

            </div>
        </div>

        <div class="swiper-slide swiper-slide-active product-content">
            <div class="list-content py-5">
                <div class="content-left">
                    <a class="name c-text-3xl font-KiaSignature-Bold">
                    </a>
                    <span class="slogan font-KiaSignature-Regular c-text-lg" style="border-bottom: 1px solid #000;">
                    </span>
                    <span class="price c-text-2xl font-KiaSignature-Bold">
                    </span>
                    <div class="_all_car">
                        <a class="black-button">Xem chi tiết</a>
                    </div>
                </div>
                <div class="content-right " style="position: relative;">
                    <div class="image-top" style="position: absolute;background-image: url(&quot;https://kiagovap.vn/themes/main/images/home/bg.png&quot;);top: -55px;width: 100%;z-index: -10;height: 70%;">
                    </div>
                    <a class="image-car d-block" style="text-align: center;">

                    </a>
                </div>
            </div>
            <div
                class="color-list swiper-pagination swiper-pagination-car swiper-pagination-clickable swiper-pagination-bullets">
            </div>
        </div>
    </div>
    </div>

</section>
