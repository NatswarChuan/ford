<section>
  <div class="mt-5 main container">
      <div class="ad-mejday my-5">
          <div class="row">
              <div class="col-md-3 mobile-hide">
                  <h3 class="mb-4">Xem nhiều</h3>
                  @foreach ($postsHot as $item)
                      <div class="row d-flex my-3">
                          <div class="col-5">
                              <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="left tin-tuc-xem-nhieu">
                                  <img src="{{ asset('storage/' . $item['image']) }}" alt=""
                                      class="img-fluid">
                              </a>
                          </div>
                          <div class="col-7">
                              <div class="ad-content-text d-block">
                                  <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="act-text title fw-bold text-uppercase text-bold">
                                      {{ $item['title'] }}</a>

                                  @php($date = date_create($item['published_date']))

                                  <p class="act-icon"><i class="fas fa-calendar-alt"></i>&nbsp;
                                      {{ date_format($date, 'd/m/Y') }}
                                  </p>
                              </div>
                          </div>
                      </div>
                  @endforeach
              </div>
              <div class="col-md-6">
                  <h3 class="mb-4">Mới nhất</h3>
                   @isset($item)
                  <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="left tin-tuc-xem-nhieu">
                      <img src="{{ asset('storage/' . $postsNew[0]['image']) }}" alt="" class="img-fluid">
                  </a>
                  <div class="ad-content-text d-block">
                      <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="act-text title fw-bold text-uppercase text-bold my-4">
                          {{ $postsNew[0]['title'] }}</a>
                    @isset($postsNew[0])
                      @php($date = date_create($postsNew[0]['published_date']))

                      <p class="act-icon"><i class="fas fa-calendar-alt"></i>&nbsp;
                          {{ date_format($date, 'd/m/Y') }}</p>
                      <p class="act-text-1 my-2 ">{{ $postsNew[0]['shortDescription'] }}</p>
                    @endIf
                  </div>
                  
                  @endIf
              </div>
              <div class="col-md-3">
                  @foreach ($postsNew as $item)
                      @if (!$loop->first)
                          <div class="mb-4">
                              <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="left tin-tuc-xem-nhieu">
                                  <img src="{{ asset('storage/' . $item['image']) }}" alt=""
                                      class="img-fluid">
                              </a>
                              <div class="ad-content-text d-block">
                                  <a href="{{route("posts.show",["slug"=>$item['slug']])}}" class="act-text title fw-bold text-uppercase text-bold my-4">
                                      {{ $item['title'] }}</a>

                                  @php($date = date_create($item['published_date']))

                                  <p class="act-icon"><i class="fas fa-calendar-alt"></i>&nbsp;
                                      {{ date_format($date, 'M d') }}
                                  </p>
                              </div>
                          </div>
                      @endif
                  @endforeach
              </div>
          </div>
      </div>
  </div>
</section>
