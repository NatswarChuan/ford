<section id="menu-desktop" class="menu menu-top d-none d-lg-block">
    <div class="container">
        <div class="menu-wrapper">
            <div class="left-menu">
                <ul class="menu">
                    <li class="menu-item hover-category">
                        <a id="modal-dang-ky-lai-thu" data-bs-toggle="modal" data-bs-target="#dang-ky-lai-thu">
                            Đăng ký lái thử
                        </a>
                    </li>
                    <li class="menu-item hover-category">
                        <a href="{{ route('posts.index') }}" target="_self">
                            Tin Tức & Khuyến Mãi
                        </a>
                    </li>
                </ul>
            </div>
            <div class="center-menu">
                <ul class="menu logo">
                    <li class="menu-item logo">
                        <a href="{{ route('home') }}" style="width:120px">
                            <img src="{{ asset('images/logo.png')}}"
                                class="img-fluid" alt="Logo">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="right-menu">
                <ul class="menu">

                    <li class="menu-item hover-category  ">
                        <a href="{{ route('page.service') }}" target="_self">
                            Dịch vụ
                        </a>
                    </li>

                    <li class="menu-item hover-category  ">
                        <a href="{{ route('product.oldCar') }}" target="_self">
                            Xe đã qua sử dụng
                        </a>
                    </li>

                    <li class="menu-item hover-category  ">
                        <a href="{{ route('page.aboutUs') }}" target="_self">
                            Liên hệ
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="menu menu-mobile d-block d-lg-none p-0">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a href="{{ route('home') }}" style="width: calc(100% - 57px);" class="text-center">
                <img src="{{ asset('images/logo.png') }}" class="img-fluid" alt="Logo" style="width: 40%!important;margin-left: calc(57px / 2);">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item p-2">
                        <a id="modal-dang-ky-lai-thu" class="nav-link active" data-bs-toggle="modal" data-bs-target="#dang-ky-lai-thu">
                            Đăng ký lái thử
                        </a>
                    </li>
                    <li class="nav-item p-2">
                        <a href="{{ route('posts.index') }}" class="nav-link active" target="_self">
                            Tin Tức & Khuyến Mãi
                        </a>
                    </li>
                     <li class="nav-item p-2">
                        <a href="{{ route('product.oldCar') }}" class="nav-link active" target="_self">
                            Xe đã qua sử dụng
                        </a>
                    </li>
                    <li class="nav-item p-2">
                        <a href="{{ route('page.service') }}" class="nav-link active" target="_self">
                            Dịch vụ
                        </a>
                    </li>
                    <li class="nav-item p-2">
                        <a href="{{ route('page.aboutUs') }}" class="nav-link active" target="_self">
                            Liên hệ
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>

@push('modal')
    @include('component.modalDangKyLaiThu')
    @include('component.modalContact')
@endpush

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
        crossorigin="anonymous"></script>
    <script>
        const testDrive = "{{ route('testDrive') }}";
        const thongTinKhachHang = "{{ route('contactContomer') }}";
    </script>

    <script src="{{ asset('js/dangKyLaiThu.js') }}"></script>
    <script src="{{ asset('js/modalContact.js') }}"></script>
@endpush
