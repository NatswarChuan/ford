@extends('layout')

@section('title')
    @isset($postsNew)
        Bài viết
    @else
        Dịch vụ
    @endif
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
@endpush

@section('content')
    <div class="cover-content">
        <div class="container">
            <div class="___news_post_content">
                <h1 class="___title">{{ $post['title'] }}</h1>
                <p class="__publish_date">{{ $post['published_date'] }}</p>

                <div class="___post_content">
                    {!! $post['content'] !!}
                </div>
            </div>
            @isset($postsNew)
                <div class="related-posts">
                    <div class="title-detail-post font-KiaSignature-Bold">
                        <h3>Tin tức liên quan</h3>
                    </div>

                    <div class="post-related-detail">
                        @each('posts.component.post-related', $postsNew, 'post')
                    </div>
                </div>
            @endisset
        </div>
    </div>
@endsection
