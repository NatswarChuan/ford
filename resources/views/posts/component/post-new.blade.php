<div class="img-post-new">
    <div class="left">
        <div class="image-wrap">
            <a href="{{ route('posts.show', ['slug' => $post['slug']]) }}">
                <img class="img-fluid" src="{{ asset('storage/' . $post['image']) }}" alt="Ford Tan Thuan">
            </a>
        </div>
    </div>
    <div class="right">
        <div class="title c-text-base">
            <a href="{{ route('posts.show', ['slug' => $post['slug']]) }}" class="c-text-dark">
                {{ $post['title'] }}
            </a>
        </div>
        @php($date = date_create($post['published_date']))

        <div class="date-time">
            <div class="date">
                <i class="far fa-calendar-alt"></i>
                <span>&nbsp; {{ date_format($date, 'd/m/Y') }}</span>
            </div>
        </div>
        <div class="content c-text-base">
            {{ $post['shortDescription'] }}
        </div>


    </div>
</div>
