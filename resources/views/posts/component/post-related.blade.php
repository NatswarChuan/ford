<div class="detail-related-articles-wrapper">
  <input type="hidden" name="id-related-post" value="856">
  <a
      href="{{route("posts.show",["slug"=>$post['slug']])}}">
      <div class="card_image-container">
          <div class="img-detail">
              <img src="{{asset('storage/'.$post['image'])}}"
                  alt="Ford Tan Thuan">
          </div>
      </div>
      <div class="car_content-container">
          <div class="car-title">
              <h2 class="title">
                  {{$post['title']}}
              </h2>
          </div>
          <div class="car-content">
              <p class="content">
                  {{$post['shortDescription']}}
              </p>
          </div>
      </div>
  </a>
</div>
