@extends('layout')

@section('title', 'Tin tức')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/tintuc.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pagination.css') }}">
@endpush

@section('content')

    <section class="new-wrapper padding-top-with-menu-sticky">
        
        <div class="cover-content">
            <div style="width: 100%;
    margin-bottom: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2px #000000 solid;">
    <span style="padding: 1rem;
    font-size: 35px;
    font-weight: bold;">Tin tức và khuyến mãi</span>
</div>
            <div class="cover-content_new" id="content-category" style="margin-top: -5%;">
                <div class="container">
                    <div class="new-category tab-content-main">
                        <div class="list-post-hot mt-8rem mb-8rem">
                            <div class="item-post-left-wrapper">
                                <div class="title-left c-text-2xl font-KiaSignature-Bold">
                                    Xem nhiều
                                </div>
                                <div class="item-post-left">
                                    @each('posts.component.post-hot', $postsNew, 'post')
                                </div>
                            </div>
                            <div class="item-post-right-wrapper">
                                <div class="title-left c-text-2xl font-KiaSignature-Bold">
                                    Tin mới
                                </div>

                                <div class="item-post-right">
                                    <div class="item-post-one w-100">
                                        <div class="img-center-post-hot">
                                            @each('posts.component.post-new', $posts, 'post')
                                        </div>
                                    </div>
                                </div>

                                {{$posts->links('pagination.pagination')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
