@extends('layout')

@section('title', 'Xe cũ')

@push('css')
<link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
@endpush

@section('content')
    <div class="container">
        <h1 class="text-center">{{ $product['name'] }}</h1>
        <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel"  style="margin-top: 50px">
            <div class="carousel-inner">
                @foreach ($product->colors as $color)
                    <div @class(['carousel-item', 'active' => $loop->first]) data-bs-interval="2000">
                        <img src="{{ asset('storage/' . $color->image) }}" style="margin: 0 auto;"  class="d-block w-75"
                            alt="...">
                    </div>
                @endforeach
            </div>
            <button class="carousel-control-prev text-dark fs-1" type="button" data-bs-target="#carouselExampleInterval"
                data-bs-slide="prev">
                <i class="fas fa-chevron-left"></i>
            </button>
            <button class="carousel-control-next text-dark fs-1" type="button" data-bs-target="#carouselExampleInterval"
                data-bs-slide="next">
                <i class="fas fa-chevron-left fa-rotate-180"></i>
            </button>
        </div>

        <h2 class="price" style="margin-top: 50px">Giá xe: <span style="color: #e63312;">{{$product['price']}} VND</span></h2>
    </div>

    <div class="cover-content">
        <div class="container">
            <div class="___news_post_content">
                <div class="___post_content">
                    {!! $product['content'] !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modal')
@endpush

@push('js')
    <script>
        const urlBaoGia = "{{ route('bao-gia') }}";
    </script>
    <script src="{{ asset('js/listProductBaoGia.js') }}"></script>
@endpush
