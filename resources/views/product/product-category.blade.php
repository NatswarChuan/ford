<div class="col-md-4">
    <div class="product-item">
        <div class="title-car">
            <div class="title-left">
                <div class="c-text-xl">
                    <h2>{{ $product['name'] }}</h2>
                </div>
            </div>
            <div class="title-right">
                @if (isset($detail))
                    <div class="item detail-product">
                        <a class="c-text-12" href="{{ route('product.show', ['slug' => $product['slug']]) }}">
                            Chi tiết
                        </a>
                    </div>
                @endif
                @if (isset($esimates))
                    <div class="item estimates-product">
                        <a type="button" class="c-text-12 button-bao-gia" data-id="{{ $product['id'] }}"
                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Báo giá
                        </a>
                    </div>
                @endif
                @if (isset($ebrochure))
                    <div @class([
                        'item',
                        'brochure-product-download' => $ebrochurePage,
                        'brochure-product' => !$ebrochurePage,
                    ])>
                        <a class="c-text-12 button-tai-ebrochure"
                            href="{{ asset('/storage/ebrochures/' . $product['ebrochure']) }}" target="_blank"
                            data-id="{{ $product['id'] }}" data-bs-toggle="modal"
                            data-bs-target="#modal-tai-ebrochure">Tải Brochure
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="price-car c-text-xl fs-4">
            <p>{{ $product['price'] }} VNĐ</p>
        </div>

        <div class="image-car">
            <a href="{{ route('product.show', ['slug' => $product['slug']]) }}">
                <img class="img-fluid" src="{{ asset('storage/' . $product['colors']['image']) }}" alt="Ford Tan Thuan">
            </a>
        </div>
    </div>
</div>
