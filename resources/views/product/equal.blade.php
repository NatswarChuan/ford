@extends('layout')

@section('title', 'So sánh')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/product-list.css') }}">
    <style>
        .main-hr {
            width: 30%;
            border: none;
            border-top: 1px solid #c3c3c3;
        }

        .icon-btn {
            width: 50px;
            height: 50px;
            border: 1px solid #cdcdcd;
            background: white;
            border-radius: 25px;
            overflow: hidden;
            position: relative;
            transition: width 0.2s ease-in-out;
        }

        .add-btn::before,
        .add-btn::after {
            transition: width 0.2s ease-in-out, border-radius 0.2s ease-in-out;
            content: "";
            position: absolute;
            height: 4px;
            width: 10px;
            top: calc(50% - 2px);
            background: red;
        }

        .add-btn::after {
            right: 14px;
            overflow: hidden;
            border-top-right-radius: 2px;
            border-bottom-right-radius: 2px;
        }

        .add-btn::before {
            left: 14px;
            border-top-left-radius: 2px;
            border-bottom-left-radius: 2px;
        }

        .icon-btn:focus {
            outline: none;
        }

        .add-icon::after,
        .add-icon::before {
            transition: all 0.2s ease-in-out;
            content: "";
            position: absolute;
            height: 20px;
            width: 2px;
            top: calc(50% - 10px);
            background: red;
            overflow: hidden;
        }

        .add-icon::before {
            left: 22px;
            border-top-left-radius: 2px;
            border-bottom-left-radius: 2px;
        }

        .add-icon::after {
            right: 22px;
            border-top-right-radius: 2px;
            border-bottom-right-radius: 2px;
        }

        .icon-remove {
            position: absolute;
            top: 0;
            right: 0;
            color: rgb(215 60 31);
            font-size: 16px;
        }

        .icon-remove:hover {
            color: #fff;
        }

        .table-so-sanh .column {
            width: 25%;
        }

        .table-so-sanh .column img {
            width: 75%;
        }


        @media only screen and (max-width: 768px) {
            .table-so-sanh .column {
                width: 50%;
            }

            .table-so-sanh span {
                display: none;
            }

            .table-so-sanh .column img {
                width: 100%;
            }

            .table-so-sanh .column .icon-remove {
                font-size: 16px;
                top: -10px;
            }
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <H1 class="text-center pt-5">So sánh sản phẩm</H1>
        <div class="table-responsive">
            <table style="table-layout:fixed; width:100%" class="table table-bordered caption-top fs-6 my-5 table-so-sanh">
                <thead>
                    <tr class="align-middle">
                        <th scope="col" class="column text-center" style="font-size: x-large;">Thông số</th>
                        @foreach ($products as $item)
                            <th scope="col" class="column">
                                @if ($item)
                                    <div class="d-flex justify-content-between align-items-center position-relative">
                                        <div class="d-flex flex-column justify-content-center align-items-center">
                                        @isset($item->colors[0])
                                            <img src="{{ asset('/storage/' . $item->colors[0]->image) }}" />
                                        @endif
                                            <span>{{ $item->name }}</span>
                                        </div>
                                        <a class="icon-remove" href="{{ route('product.equal.remove', [$item->id]) }}">
                                            <i class="fas fa-window-close"></i>
                                        </a>
                                    </div>
                                @else
                                    <div class="w-100 d-flex justify-content-center align-items-center">
                                        <button class="icon-btn add-btn" data-bs-toggle="modal" data-bs-target="#so-sanh">
                                            <div class="add-icon"></div>
                                        </button>
                                    </div>
                                @endif
                            </th>
                        @endforeach
                    </tr>
                </thead>
                @php
                    $attributes = [];
                    foreach ($products as $item) {
                        if ($item) {
                            foreach ($item['tables'] as $table) {
                                $attributes[$table->title] = true;
                            }
                        }
                    }
                    uksort($attributes, function($a, $b) {
    $number_a = intval(explode('.', $a)[0]);
    $number_b = intval(explode('.', $b)[0]);
    return strnatcmp($number_a, $number_b);
});

                @endphp
                <tbody>
                    @foreach ($attributes as $attribute => $value)
                        <tr>
                        <th scope="row">{{ $attribute }}</th>
                            @foreach ($products as $item)
                                @php
                                    $attributeValue = '';
                                    if ($item) {
                                        foreach ($item->tables as $table) {
                                            if ($table->title === $attribute) {
                                                $attributeValue = $table->content;
                                                break;
                                            }
                                        }
                                    }
                                @endphp
                                <td>{{ $attributeValue }}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('modal')
    @include('product.component.modal-so-sanh')
@endpush
