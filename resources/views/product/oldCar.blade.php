@extends('layout')

@section('title', 'Xe đã qua sử dụng')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/product-list.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pagination.css') }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div style="width: 100%;
    margin-bottom: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2px #000000 solid;">
    <span style="padding: 1rem;
    font-size: 35px;
    font-weight: bold;">Xe đã qua sử dụng</span>
</div>
        <div class="product-content-new">
            <div class="product-content">
                <div class="page-product-car">
                     @if(count($products) > 0)
                    <form name="CarLineFilter" method="GET">
                        <div class="list-product-car use-loading">
                           
                            <div class="row">
                                @foreach ($products as $product)
                                    <div class="col-md-4">
                                        <div class="product-item">
                                            <div class="title-car">
                                                <div class="title-left">
                                                    <div class="c-text-xl">
                                                        <h2>{{ $product['name'] }}</h2>
                                                    </div>
                                                </div>
                                                <div class="title-right row2">
                                                    <div class="item detail-product">
                                                        <a class="c-text-12"
                                                            href="{{ route('product.oldCarShow', ['slug' => $product['slug']]) }}">
                                                            Chi tiết
                                                        </a>
                                                    </div>
                                                    <div class="item estimates-product">
                                                        <a type="button" class="c-text-12 button-bao-gia"
                                                            data-id="{{ $product['id'] }}" data-bs-toggle="modal"
                                                            data-bs-target="#exampleModal">
                                                            Báo giá
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-car c-text-xl fs-4">
                                                <p>{{ $product['price'] }} VNĐ</p>
                                            </div>

                                            <div class="image-car">
                                                <a href="{{ route('product.oldCarShow', ['slug' => $product['slug']]) }}">
                                                    <img class="img-fluid"
                                                        src="{{ asset('storage/' . $product->colors[0]->image) }}"
                                                        alt="Ford Tan Thuan">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                {{ $products->links('pagination.pagination') }}
                            </div>
                           
                        </div>
                    </form>
                     @else
                        <div style="
    height: 75vh;
    display: table-cell;
    vertical-align: middle;
"><H1 class="text-center" style="
    width: 100vw;
">Chúng tôi hiện chưa có sản phẩm đã qua sử dụng</H1></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modal')
    @include('product.component.modal-bao-gia')
@endpush

@push('js')
    <script>
        const urlBaoGia = "{{ route('bao-gia') }}";
    </script>
    <script src="{{ asset('js/listProductBaoGia.js') }}"></script>
@endpush
