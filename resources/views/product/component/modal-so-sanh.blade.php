<div class="modal fade w-100" id="so-sanh" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <!-- Nội dung của modal -->
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chọn sản phẩm</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Đóng"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @foreach (App\Models\Product::getAllProductEqual() as $item)
                        <div class="col-md-4">
                            <div class="card mb-3">
                                <a href="{{ route('product.equal', ['slug' => $item->slug]) }}">
                                    @isset($item->colors[0])
                                    <img src="{{ asset('/storage/' . $item->colors[0]->image) }}"
                                        class="card-img-top my-2" alt="{{ $item['name'] }}">
                                        @endif
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $item['name'] }}</h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
