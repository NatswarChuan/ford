<div class="modal fade w-100" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form id="form-bao-gia" onsubmit="return false">
                <!-- Nội dung của modal -->
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Báo giá</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Đóng"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="product_id" class="form-control" id="product_id">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tên</label>
                                <input type="text" name="full_name" class="form-control" id="name"
                                    placeholder="Nhập tên" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="email"
                                    placeholder="Nhập email" required>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Số điện thoại</label>
                        <input type="tel" name="phone_number" class="form-control" id="phone"
                            placeholder="Nhập số điện thoại" pattern="[0-9]{10,11}" required>
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">Địa chỉ</label>
                        <input type="address" name="address" class="form-control" id="address"
                            placeholder="Nhập địa chỉ" required>
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Nội dung(nếu có)</label>
                        <textarea type="description" name="description" class="form-control" id="description" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div id="alert-container"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('js/modal.js') }}"></script>
@endpush
