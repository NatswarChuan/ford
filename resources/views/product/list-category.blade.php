<div class="dong">
    <span>{{ $category['name'] }}</span>
</div>
<div class="row w-100 ms-0">
    @foreach ($category['products'] as $product)
        @if (isset($ebrochure))
            @include('product.product-category', [
                'product' => $product,
                'ebrochure' => true,
                'ebrochurePage' => true,
            ])
        @else
            @include('product.product-category', [
                'product' => $product,
                'ebrochure' => true,
                'detail' => true,
                'esimates' => true,
                'ebrochurePage' => false,
            ])
        @endif
    @endforeach
</div>
