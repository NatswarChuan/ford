@extends('layout')

@section('title', 'Sản Phẩm')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/product-list.css') }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="product-content-new">
            <div class="product-content">
                <div class="page-product-car">
                    <form name="CarLineFilter" method="GET">
                        <div class="list-product-car use-loading">
                            @each('product.list-category', $products, 'category')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modal')
    @include('product.component.modal-bao-gia')
    @include('product.component.modalTaiEbrochure')
@endpush

@push('js')
    <script>
        const urlBaoGia = "{{ route('bao-gia') }}";
        const urlTaiEbrochure = "{{ route('ebrochure') }}";
    </script>
    <script src="{{ asset('js/listProductBaoGia.js') }}"></script>
    <script src="{{ asset('js/modalTaiEbrochure.js') }}"></script>
@endpush
