@extends('layout')

@section('title', 'Trả góp')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div
            style="width: 100%;
    margin-bottom: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border: 2px #000000 solid;">
            <span style="padding: 1rem;
    font-size: 35px;
    font-weight: bold;">Đăng ký mua Ford trả góp</span>
        </div>

        <div class="cover-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div>{!! $store->credit !!}</div>
                    </div>
                    <div class="col-md-5" style="margin-bottom: 5%">
                        <form method="get" onsubmit="return false;"
                            class="d-flex justify-content-center flex-column align-items-center form-credit">
                            <div class="h2">ĐĂNG KÍ MUA FORD TRẢ GÓP</div>
                            <div class=" w-50">
                                <div class="mb-3">
                                    <label for="full-name" class="form-label">Họ và tên</label>
                                    <input type="text" class="form-control" name="full_name" id="full-name" required />
                                </div>


                                <div class="mb-3">
                                    <label for="phone-number" class="form-label">Số điện thoại</label>
                                    <input type="text" id="phone-number" name="phone_number" class="form-control"
                                        pattern="[0-9]{10,11}" required />
                                </div>

                                <div class="mb-3">
                                    <label for="product" class="form-label">Vui lòng chọn xe</label>
                                    <select id="product" name="product_id" required class="form-control">
                                        <option disabled selected></option>
                                        @foreach ($products as $product)
                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Đăng ký</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="___news_post_content">
                    <h1 class="___title">{{ $post['title'] }}</h1>
        
        <img src="{{ asset("/storage/".$post['image']) }}" class="w-100 img-fluig">

                    <div class="___post_content">
                        {!! $post['content'] !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        const urlCredit = "{{ route('credit') }}";
    </script>
    <script src="{{ asset('js/credit.js') }}"></script>
@endpush
