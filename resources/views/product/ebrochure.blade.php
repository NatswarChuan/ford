@extends('layout')

@section('title', 'Sản Phẩm')

@push('css')
    <link rel="stylesheet" href="{{ asset('css/product-list.css') }}">
@endpush

@section('content')
    <div class="container-fluid">
        <div class="product-content-new">
            <div class="product-content">
                <div class="page-product-car">
                    <form name="CarLineFilter" method="GET">
                        <div class="list-product-car use-loading">
                            @foreach ($categories as $category)
                                @include('product.list-category', [
                                    'category' => $category,
                                    'ebrochure' => true,
                                ])
                            @endforeach
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('modal')
    @include('product.component.modalTaiEbrochure')
@endpush

@push('js')
    <script>
        const urlTaiEbrochure = "{{ route('ebrochure') }}";
    </script>
    <script src="{{ asset('js/modalTaiEbrochure.js') }}"></script>
@endpush
