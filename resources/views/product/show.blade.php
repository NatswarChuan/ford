@extends('/layout')

@section('title', $product['name'])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select-car.css') }}">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
    <link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/baiviet.css') }}">
    <style>
        img {
            max-width: 100%;
        }

        .price {
            color: #e63312;
        }
    </style>
@endpush
@section('content')
    <section class="___estimate_wrapper ___select_car_wrapper">
        <div class="container">
            <div class="___content_left">
                <div id="estimate-car" class="h-100">
                    <h2 class="text-center">{{ $product['name'] }}</h2>
                    <div class="slide-wrapper ">
                        <div
                            class="swiper-container ___select_car_container _swiper_color_ajax swiper-container-initialized swiper-container-horizontal">
                            <div class="swiper-wrapper highlight">
                                <div class="swiper-slide slide-132 image-car" style="justify-content: center;">
                                    @foreach ($product['colors'] as $item)
                                        @if ($loop->first)
                                            <img src="{{ asset('/storage/' . $item['image']) }}" class="image-car-pick"
                                                data-color-id="{{ $item['id'] }}" alt="Car" class="img-fluid">
                                        @else
                                            <img src="{{ asset('/storage/' . $item['image']) }}"
                                                class="hidden image-car-pick" data-color-id="{{ $item['id'] }}"
                                                alt="Car" class="img-fluid">
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Nhớ gửi mảng color sang js để renders -->
                    <div
                        class="color-list swiper-pagination swiper-pagination-car swiper-pagination-clickable swiper-pagination-bullets">
                        @foreach ($product['colors'] as $item)
                            @if ($loop->first)
                                <span class="swiper-pagination-bullet  color-pick swiper-pagination-bullet-active"
                                    data-color-id="{{ $item['id'] }}" style="--data-color:{{ $item['color_code'] }}"
                                    tabindex="0" value="{{ $item['name'] }}"></span>
                            @else
                                <span class="swiper-pagination-bullet  color-pick" data-color-id="{{ $item['id'] }}"
                                    style="--data-color:{{ $item['color_code'] }}" tabindex="0"
                                    value="{{ $item['name'] }}"></span>
                            @endif
                        @endforeach

                    </div>
                    @if ($product->colors->first())
                        <div class="pt-5 text-center" style="color:#05141f;font-size: xx-large;" id="color-name">
                            <span>{{ $product->colors->first()->name }}</span></div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="d-flex fs-3 justify-content-between my-3 col-md-4 mt-1">
                    <span>Giá xe: </span>
                    <span class="price">{{ $product['price'] }} VNĐ</span>
                </div>
                <div class="col-md-4 mt-1">
                    <div class="d-flex justify-content-center">
                        <button class="black-button button-bao-gia fs-6" data-id="{{ $product['id'] }}"
                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Báo giá lăn bánh
                        </button>
                    </div>
                </div>
                <div class="col-md-4 mt-1">
                    <div class="d-flex justify-content-center">
                        <a class="black-button button-bao-gia fs-6"
                            href="{{ route('product.equal', ['slug' => $product['slug']]) }}">
                            So sánh
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    
      <!-- bài giới thiệu -->
    <section>
        <div class="container">
            <div class="fs-6">{!! $product['content'] !!}</div>
        </div>
    </section>

    <!-- Thông số xe -->
    <section class="mb-5">
        <div class="container">
            <table class="table table-bordered caption-top fs-6">
                <caption style="font-size: 2rem;">Thông số xe</caption>
                <tbody>
                    @foreach ($product['tables'] as $item)
                        <tr>
                            <th scope="row">{{ $item['title'] }}</th>
                            <td>{{ $item['content'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </section>
    
  
@endsection

@push('modal')
    @include('product.component.modal-bao-gia')
@endpush

@push('js')
    <script>
        const urlBaoGia = "{{ route('bao-gia') }}";

        const colorPick = document.querySelectorAll(".color-pick");
        const imageCar = document.querySelector(".image-car");

        colorPick.forEach(colorElement => {
            colorElement.addEventListener('click', (e) => {
                colorPick.forEach(colorP => {
                    colorP.classList.remove("swiper-pagination-bullet-active");
                })
                document.querySelectorAll('.image-car-pick').forEach((imageCarPick) => {
                    imageCarPick.classList.remove("hidden");
                    imageCarPick.classList.add("hidden");
                })
                e.target.classList.add("swiper-pagination-bullet-active")
                imageCar.querySelector(`[data-color-id="${e.target.getAttribute("data-color-id")}"]`)
                    .classList.remove("hidden");
                document.getElementById("color-name").innerHTML = e.target.getAttribute("value");
            })
        });
        
        document.querySelector(".zalo").style.bottom = 0;
    </script>
    <script src="{{ asset('js/productBaoGia.js') }}"></script>
@endpush
