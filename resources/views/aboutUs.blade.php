<script src="{{ asset('js/app.js') }}"></script>
<script>
    const data = {
        full_name: "John Doe",
        email: "johndoe@example.com",
        phone_number: "123456789",
        address: "123 Street, City"
    };

    // Gửi yêu cầu POST
    window.axios.post('http://localhost:8000/api/thong-tin-khach-hang', data)
        .then(response => {
            console.log('Success:', response.data);
        })
        .catch(error => {
            console.error('Error:', error);
        });
</script>
