<div class="modal fade w-100" id="modal-contact" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <form id="form-dang-ky-lai-thu" onsubmit="return false">
              <!-- Nội dung của modal -->
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Thông tin liên hệ</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Đóng"></button>
              </div>
              <div class="modal-body">

                          <div class="mb-3">
                              <label for="name" class="form-label">Tên</label>
                              <input type="text" name="full_name" class="form-control" required id="name"
                                  placeholder="Nhập tên">
                          </div>
                  <div class="mb-3">
                      <label for="phone" class="form-label">Số điện thoại</label>
                      <input type="tel" name="phone_number"  pattern="[0-9]{10,11}" class="form-control" required id="phone"
                          placeholder="Nhập số điện thoại">
                  </div>
                  <div class="mb-3">
                      <label for="address" class="form-label">Địa chỉ</label>
                      <input type="address" name="address" class="form-control" required id="address"
                          placeholder="Nhập địa chỉ">
                  </div>
                  <div class="mb-3">
                      <label for="description" class="form-label">Nội dung(nếu có)</label>
                      <textarea type="description" name="description" class="form-control" id="description" placeholder="Nhập nội dung"></textarea>
                  </div>
                  <div id="alert-container-contact"></div>
              </div>
              <div class="modal-footer justify-content-center">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                  <button type="submit" class="btn btn-primary">Gửi</button>
              </div>
          </form>
      </div>
  </div>
</div>
