<?php

namespace Database\Seeders;

use App\Models\CustomerContact;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CustomerContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($j = 0; $j < 3; $j++) {
            for ($i = 1; $i <= 20; $i++) {
                $product = Product::inRandomOrder()->first();
                $time = Carbon::now()->subDays(rand(0, 30));
                CustomerContact::create([
                    'full_name' => 'Customer Contact ' . $i,
                    'email' => 'contact' . $i . '@example.com',
                    'phone_number' => '123456789',
                    'address' => 'Address ' . $i,
                    'description' => 'Description ' . $i,
                    'product_id' => $j != CustomerContact::CONTACT ? $product->id : null,
                    'time' => $j != CustomerContact::CONTACT ? $time : null,
                    'type' => $j,
                ]);
            }
        }
    }
}
