<?php
namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {
        for ($i = 1; $i <= 20; $i++) {
            Product::create([
                'name' => 'Product ' . $i,
                'slug' => 'product-' . $i,
                'version' => '1.0',
                'content' => '<div class="___news_post_content"><h1 class="___title">Bảo dưỡng đón hè - Nhận ngay ưu đãi từ trung tâm dịch vụ Kia</h1><p class="__publish_date">30/05/2023</p><div class="___post_content"><p style="margin-bottom:0in"><span style="line-height:normal"><span arial="" style="font-family:"><span style="color:black">Với triết lý “Tận tâm phục vụ”, Kia Việt Nam luôn cố gắng mang đến những giá trị thiết thực dành cho Khách hàng. Vì vậy, trong tháng 6 này, Kia Việt Nam triển khai chương trình “</span><strong><a href="https://kiavietnam.com.vn/bao-duong-dinh-ky"><span style="color:#000000;">BẢO DƯỠNG ĐÓN HÈ, NHẬN NGAY ƯU ĐÃI</span></a></strong><span style="color:black">” với những đặc quyền khi sử dụng dịch vụ tại Trung tâm dịch vụ chính hãng Kia Việt Nam. </span></span></span></p><p style="margin-bottom:0in">&nbsp;</p><p style="margin-bottom:0in"><img alt="Banner web-01-1" src="https://kiavietnam.com.vn/storage/banner-web-01-1.png"></p><p style="margin-bottom:0in">&nbsp;</p><p style="margin-bottom:0in"><span style="line-height:normal"><span arial="" style="font-family:"><span style="color:black">Từ ngày 01/06 đến hết 30/06/2023, tất cả khách hàng đưa xe vào xưởng làm dịch vụ nếu </span><strong><a href="https://kiavietnam.com.vn/bao-duong-dinh-ky"><span style="color:#000000;">có đặt hẹn trước và đến đúng thời gian đã hẹn</span></a></strong><span style="color:black"> sẽ được tặng ngay voucher dịch vụ chăm sóc xe. Khách hàng được chọn 1 trong 2 dịch vụ chăm sóc xe sau: Tẩy ổ kính (hoặc) Đánh bóng chi tiết Chrome.</span></span></span></p><p style="margin-bottom:0in">&nbsp;</p><p style="margin-bottom:0in"><img alt="DVPT-1" src="https://kiavietnam.com.vn/storage/dvpt-1.jpg"></p><p style="margin-bottom:0in">&nbsp;</p><p style="margin-bottom:0in"><span style="line-height:normal"><span arial="" style="font-family:"><span style="color:black">Đặc biệt, cũng trong tháng 6 này, Kia Việt Nam dành sự quan tâm đến những khách hàng đã lâu chưa có dịp đưa xe vào xưởng sử dụng các dịch vụ bảo dưỡng hay chăm sóc xe thông qua việc tặng gói kiểm tra xe miễn phí và nhiều ưu đãi hấp dẫn khi thay dầu động cơ. Đây là dịp để Kia Việt Nam tri ân và chăm sóc xe cho các khách hàng, giúp xe luôn hoạt động tốt nhất và kéo dài tuổi thọ.</span></span></span></p><p style="margin-bottom:0in">&nbsp;</p><p style="margin-bottom:0in"><span style="line-height:normal"><span arial="" style="font-family:"><span style="color:black">Để tìm hiểu thêm về chương trình, Quý khách vui lòng liên hệ </span><strong><a href="https://kiavietnam.com.vn/lien-he"><span style="color:#000000;">Hotline 1900 545 591</span></a></strong><span style="color:black"> hoặc </span><strong><a href="https://kiavietnam.com.vn/dai-ly"><span style="color:#000000;">các Trung tâm dịch vụ chính hãng Kia</span></a></strong><span style="color:black"> gần nhất.</span></span></span></p></div></div>',
                'price' => rand(10, 100),
                'ebrochure' => 'product_' . $i . '.pdf',
                'status' => Product::ACTIVE,
                'category_id' => rand(1, 5),
            ]);
        }

    }
}
