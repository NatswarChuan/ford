<?php

namespace Database\Seeders;

use App\Models\Table;
use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($j = 1; $j <= 20; $j++) {
            $numTables = rand(3, 5);
            for ($i = 0; $i < $numTables; $i++) {
                Table::create([
                    'title' => 'Table ' . ($i + 1),
                    'content' => 'Content ' . ($i + 1),
                    'product_id' => $j,
                ]);
            }
        }
    }
}
