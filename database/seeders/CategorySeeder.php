<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $categories = [
            ['name' => 'Everest', 'status' => 1],
            ['name' => 'Explorer', 'status' => 1],
            ['name' => 'Ranger', 'status' => 1],
            ['name' => 'Raptor', 'status' => 1],
            ['name' => 'Territory', 'status' => 1],
            ['name' => 'Transit', 'status' => 1],
        ];

        Category::insert($categories);
    }
}
