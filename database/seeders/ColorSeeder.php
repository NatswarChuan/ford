<?php

namespace Database\Seeders;

use App\Models\Color;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        foreach ($products as $product) {
            $numColors = rand(3, 5);

            for ($i = 1; $i <= $numColors; $i++) {
                $color = new Color([
                    'color_code' => $this->generateRandomColorCode(),
                    'image' => 'image' . $i . '.jpg',
                ]);
                $product->colors()->save($color);
            }
        }
    }

    private function generateRandomColorCode()
    {
        $letters = '0123456789ABCDEF';
        $colorCode = '#';

        for ($i = 0; $i < 6; $i++) {
            $index = rand(0, 15);
            $colorCode .= $letters[$index];
        }

        return $colorCode;
    }
}
