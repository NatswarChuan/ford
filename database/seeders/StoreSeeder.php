<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::create([
            'name' => '𝐏𝐇𝐔́ 𝐌𝐘̃ 𝐅𝐎𝐑𝐃',
            'address' => '2, Trạm thu phí cầu Phú Mỹ, đường C2/Lô B1 TP. Thủ Đức, Khu Công Nghiệp, Quận 2, Thành phố Hồ Chí Minh, Việt Nam',
            'phone' => '0975358752',
            'email' => 'mail@example.com',
            'iframe' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15673.3371416394!2d106.7588497!3d10.862159199999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d87f75730689%3A0x3a62bc942b039c39!2zQ2jhu6MgVGjhu6cgxJDhu6lj!5e0!3m2!1svi!2s!4v1687198279114!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>'
        ]);
    }
}
