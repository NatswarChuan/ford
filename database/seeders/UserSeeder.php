<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'master',
            'email' => 'nguyenducford@gmail.com',
            'password' =>  bcrypt('admin'), 
            'role' =>  'master', 
        ]);
    }
}
