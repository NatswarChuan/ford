const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);


mix.sass('resources/scss/app.scss', 'public/css', {
    implementation: require('node-sass')
});
mix.sass('resources/scss/footer1.scss', 'public/css', {
    implementation: require('node-sass')
});
mix.sass('resources/scss/select-car.scss', 'public/css', {
    implementation: require('node-sass')
});
mix.sass('resources/scss/product-list.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/tintuc.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/baiviet.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/pagination.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/menu.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/form.scss', 'public/css', {
    implementation: require('node-sass')
});

mix.sass('resources/scss/box.scss', 'public/css', {
    implementation: require('node-sass')
});


mix.sass('resources/scss/checkbox-custom.scss', 'public/css', {
    implementation: require('node-sass')
});

