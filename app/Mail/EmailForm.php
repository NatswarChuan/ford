<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\CustomerContact;

class EmailForm extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;
    public $title = [
        "0" => 'Khách hàng đăng ký lái thử',
        "1" => 'Khách hàng ghé vào trang web',
        "2" => 'Khách hàng nhận báo giá',
        "3" => 'Khách hàng tải ebrochure',
        "4" => 'Khách hàng đăng ký trả góp'
    ];

    /**
     * Create a new message instance.
     *
     * @param  CustomerContact  $contact
     * @return void
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails')->subject($this->title[$this->contact->type]);
    }
}
