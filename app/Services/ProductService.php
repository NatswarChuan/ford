<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Product;

class ProductService
{
    public static function getProductForHomePage()
    {
        $categories = Category::all();
        foreach ($categories as $category){
            $category->setAttribute('products',$category->products()->where('type',Product::NEW)->where('status',Product::ACTIVE)->get());
            foreach($category->products as $product){
                $product->setAttribute('colors',$product->colors()->get());
            }
        }
        return $categories;
    }

    public static function getProductBySlug($slug)
    {
        $product = Product::where('slug',$slug)->where('type',Product::NEW)->where('status',Product::ACTIVE)->get(['id','name','slug','version','price','ebrochure'])->first();
        if($product){
            $product->setAttribute('colors',$product->colors()->get()->reverse()->values());
        }
        return $product;
    }

    public static function getProductForEbrochurePage()
    {
        $categories = Category::all();
        foreach ($categories as $category){
            $category->setAttribute('products',$category->products()->where('type',Product::NEW)->where('status',Product::ACTIVE)->get());
            foreach($category->products as $product){
                $product->setAttribute('colors',$product->colors()->first());
            }
        }
        return $categories;
    }

    public static function getProductDetailBySlug($slug)
    {
        $product = Product::where('slug',$slug)->where('type',Product::NEW)->where('status',Product::ACTIVE)->get()->first();
        if($product){
            $product->setAttribute('colors',$product->colors()->get());
            $product->setAttribute('tables',$product->tables()->orderByRaw("CAST(SUBSTRING_INDEX(title, '.', 1) AS UNSIGNED)")->get(['title'	,'content']));
        }
        return $product;
    }

}
