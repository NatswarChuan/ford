<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Product;
class CategoryService
{
    public static function getCategoryForHomePage()
    {
        $categories = Category::where('status',Category::ACTIVE)->get();
        foreach ($categories as $category){
            $category->setAttribute('products',$category->products()->where('status',Product::ACTIVE)->where('type',Product::NEW)->get(['slug','name']));
        }
        return $categories;
    }

    public static function getCategoryForEbrochurePage(){
        $categories = Category::where('status',Category::ACTIVE)->get(['id','name']);
        foreach ($categories as $category){
            $category->setAttribute('products',$category->products()->where('status',Product::ACTIVE)->get());
            foreach($category->products as $product){
                $product->setAttribute('colors',$product->colors()->get()->firstOrFail());
            }
        }
        return $categories;
    }
}
