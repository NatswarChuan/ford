<?php
namespace App\Services;

use App\Models\Post;

class PostService
{
    public static function getService()
    {
        $service = Post::where('type',Post::SERVICE)->where('status',Post::ACTIVE)->latest()->firstOrFail();
        return $service;
    }

    public static function getPostsSortByViewsForHomePage()
    {
        $posts = Post::where('type',Post::POST)->where('status',Post::ACTIVE)->orderByDesc('views')->latest()->take(6)->get();
        foreach($posts as $post)   {
            $post->setAttribute('shortDescription',$post->getShortDescriptionAttribute());
        }
        return $posts;
    }
    public static function getPostsNewForHomePage()
    {
        $posts = Post::where('type',Post::POST)->where('status',Post::ACTIVE)->orderByDesc('views')->orderByDesc('published_date')->take(3)->get();
        foreach($posts as $post)   {
            $post->setAttribute('shortDescription',$post->getShortDescriptionAttribute());
        }
        return $posts;
    }
}
