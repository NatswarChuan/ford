<?php
namespace App\Services;

use App\Models\Category;
use Illuminate\Http\Request;

class FileService
{
    public static function saveIamge(Request $request)
    {
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            return '/images/'.$name;
        }
        return null;
    }
}
