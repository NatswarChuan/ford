<?php

namespace App\Services;

use App\Models\CustomerContact;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailForm;
use App\Models\Store;

class CustomerContactService
{
    public static function createContact($full_name, $email, $phone_number, $address, $type, $description = null, $product_id = null, $time = null)
    {
        $contact = new CustomerContact();
        $contact->full_name = $full_name;
        $contact->email = $email;
        $contact->phone_number = $phone_number;
        $contact->address = $address;
        $contact->product_id = $product_id;
        $contact->description = $description;
        $contact->time = $time;
        $contact->type = $type;
        $contact->save();

        Mail::to(Store::all()->first()->email)->send(new EmailForm($contact));
    }
}
