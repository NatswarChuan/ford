<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;
    const ACTIVE = true;
    const NEW = 1;
    const OLD = 2;

    public function getShortDescriptionAttribute()
    {
        return Str::limit(strip_tags($this->content), 200);
    }

    public function colors()
    {
        return $this->hasMany(Color::class);
    }

    public function tables()
    {
        return $this->hasMany(Table::class);
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    function genarateSlug()
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $this->name);
        }
        $str = str_replace(' ', '-', $str);
        $str = str_replace('/', '-', $str);
        $this->slug = $str;
    }

    public static function getAllProductActive()
    {
        return Product::where('status', Product::ACTIVE)->get(['id', 'name', 'slug']);
    }

    public static function getAllProductEqual()
    {
        if (session()->has('products')) {
            if (count(session()->get('products')) > 0) {
                $category_id = current(session()->get('products'))->category()->first()->id;
                return Product::where('category_id', $category_id)->where('status', Product::ACTIVE)->get(['id', 'name', 'slug']);
            }
        }
        return [];
    }
}
