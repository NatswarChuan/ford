<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model
{
    use HasFactory;

    const TEST_DRIVE = 0;
    const CONTACT = 1;
    const QUOTATION = 2;
    const EBROCHURE = 3;
    const CREDIT = 4;

    protected $fillable = [
        'full_name',
        'email',
        'phone_number',
        'address',
        'product_id',
        'description',
        'type',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
