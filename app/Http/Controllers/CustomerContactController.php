<?php

namespace App\Http\Controllers;

use App\Models\CustomerContact;
use App\Models\Product;
use App\Services\CustomerContactExport;
use App\Services\CustomerContactService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use function PHPSTORM_META\type;
use function PHPUnit\Framework\isEmpty;

class CustomerContactController extends Controller
{
    public function contactContomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'description' => 'nullable',
        ]);

        $full_name = $request->full_name;
        $email = '';
        $phone_number = $request->phone_number;
        $address = $request->address;
        $description = $request->description;
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }

        CustomerContactService::createContact($full_name, $email, $phone_number, $address, CustomerContact::CONTACT, $description);
        return response()->json([
            'message' => 'Customer contact saved successfully',
        ], 201);
    }

    public function credit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone_number' => 'required',
            'product_id' => 'required|numeric|min:1',
        ]);

        $full_name = $request->full_name;
        $phone_number = $request->phone_number;
        $product_id = $request->product_id;
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }

        CustomerContactService::createContact($full_name, null, $phone_number, null, CustomerContact::CREDIT, null, $product_id, null);

        return response()->json([
            'message' => 'Chúng tôi sẽ liên hệ lại với bạn sau!!!',
        ], 201);
    }

    public function testDrive(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'description' => 'nullable',
            'time' => 'required|date',
            'product_id' => 'required|numeric|min:1',
        ]);

        $full_name = $request->full_name;
        $email = '';
        $phone_number = $request->phone_number;
        $address = $request->address;
        $description = $request->description;
        $product_id = $request->product_id;
        $time = $request->time;
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }

        CustomerContactService::createContact($full_name, $email, $phone_number, $address, CustomerContact::TEST_DRIVE, $description, $product_id, $time);

        return response()->json([
            'message' => 'Customer testDrive saved successfully',
        ], 201);
    }

    public function quotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'description' => 'nullable',
            'product_id' => 'required|numeric|min:1',
        ]);

        $full_name = $request->full_name;
        $email = '';
        $phone_number = $request->phone_number;
        $address = $request->address;
        $description = $request->description;
        $product_id = $request->product_id;
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }

        CustomerContactService::createContact($full_name, $email, $phone_number, $address, CustomerContact::QUOTATION, $description, $product_id);

        return response()->json([
            'message' => 'Quotation saved successfully',
        ], 201);
    }
    public function ebrochure(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'product_id' => 'required|numeric|min:1',
        ]);

        $full_name = $request->full_name;
        $email = '';
        $phone_number = $request->phone_number;
        $address = $request->address;
        $product_id = $request->product_id;
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation failed',
                'errors' => $validator->errors(),
            ], 422);
        }

        CustomerContactService::createContact($full_name, $email, $phone_number, $address, CustomerContact::EBROCHURE, '', $product_id);

        return response()->json([
            'message' => 'Ebrochure saved successfully',
            'link'=>asset('/storage/ebrochures/' . Product::find($product_id)->ebrochure)
        ], 201);
    }

    function index(Request $request)
    {
        $query = CustomerContact::where('type', CustomerContact::CONTACT);

        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $query->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $query->whereDate('created_at', '<=', $endDate);
        }

        $customers = $query->orderByDesc('created_at')->paginate(12);
        return view('admin.customer.index', ['customers' => $customers]);
    }

    function showTestDrive(Request $request)
    {
        $query = CustomerContact::where('type', CustomerContact::TEST_DRIVE);

        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $query->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $query->whereDate('created_at', '<=', $endDate);
        }

        $customers = $query->orderByDesc('created_at')->paginate(12);
        return view('admin.customer.testDrive', ['customers' => $customers]);
    }

    function showEbrochure(Request $request)
    {
        $query = CustomerContact::where('type', CustomerContact::EBROCHURE);

        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $query->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $query->whereDate('created_at', '<=', $endDate);
        }

        $customers = $query->orderByDesc('created_at')->paginate(12);
        return view('admin.customer.ebrochure', ['customers' => $customers]);
    }
    function showQuotation(Request $request)
    {
        $query = CustomerContact::where('type', CustomerContact::QUOTATION);

        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $query->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $query->whereDate('created_at', '<=', $endDate);
        }

        $customers = $query->orderByDesc('created_at')->paginate(12);
        return view('admin.customer.quotation', ['customers' => $customers]);
    }

    function exportContact(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = CustomerContact::where('type', CustomerContact::CONTACT);
        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $data->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $data->whereDate('created_at', '<=', $endDate);
        }
        $data = $data->orderByDESC('created_at')->get();

        $spreadsheet = new Spreadsheet();

        // Lấy sheet hiện tại
        $sheet = $spreadsheet->getActiveSheet();

        // Đặt dòng đầu tiên là tiêu đề và định dạng
        $sheet->setCellValue('A1', 'Họ và tên');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'Số điện thoại');
        $sheet->setCellValue('D1', 'Địa chỉ');
        $sheet->setCellValue('E1', 'Nội dung');
        $sheet->setCellValue('F1', 'Ngày tạo');

        // Định dạng font cho hàng đầu tiên (size 14, đậm)
        $sheet->getStyle('A1:F1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        // Lặp qua dữ liệu và đặt giá trị vào các ô tương ứng
        $row = 2; // Dòng bắt đầu từ 2
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $row, $item->full_name);
            $sheet->setCellValue('B' . $row, $item->email);
            $sheet->setCellValue('C' . $row, $item->phone_number);
            $sheet->setCellValue('D' . $row, $item->address);
            $sheet->setCellValue('E' . $row, $item->description);
            $sheet->setCellValue('F' . $row, $item->created_at->format('d/m/Y'));
            // Đóng khung các ô có dữ liệu
            $sheet->getStyle('A' . $row . ':F' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
            ]);
            $row++;
        }

        // Đặt kích thước font cho hàng còn lại (size 11)
        $sheet->getStyle('A2:F' . $row)->applyFromArray([
            'font' => [
                'size' => 11,
            ],
        ]);

        // Đặt chiều rộng các cột dựa trên chiều dài nội dung
        $lastColumn = $sheet->getHighestColumn();
        $columnIndex = 'A';

        while ($columnIndex <= $lastColumn) {
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }
        $sheet->getStyle('A1:F' . ($row - 1))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        //Tạo filter
        $sheet->setAutoFilter('F1:F' . ($row - 1));

        // Tạo một đối tượng Writer để ghi file Excel
        $writer = new Xlsx($spreadsheet);
        $fileName = 'ThongTinKhachHang.' . $startDate . '.' . $endDate . '.xlsx';

        // Thiết lập header của response để trình duyệt tải xuống file Excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        // Ghi file Excel vào output buffer
        $writer->save('php://output');
    }

    function exportTestDrive(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = CustomerContact::where('type', CustomerContact::TEST_DRIVE);
        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $data->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $data->whereDate('created_at', '<=', $endDate);
        }
        $data = $data->orderByDESC('created_at')->get();

        $spreadsheet = new Spreadsheet();

        // Lấy sheet hiện tại
        $sheet = $spreadsheet->getActiveSheet();

        // Đặt dòng đầu tiên là tiêu đề và định dạng
        $sheet->setCellValue('A1', 'Họ và tên');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'Số điện thoại');
        $sheet->setCellValue('D1', 'Địa chỉ');
        $sheet->setCellValue('E1', 'Nội dung');
        $sheet->setCellValue('F1', 'Sản phẩm');
        $sheet->setCellValue('G1', 'Ngày đăng ký lái thử');
        $sheet->setCellValue('H1', 'Ngày tạo');


        // Định dạng font cho hàng đầu tiên (size 14, đậm)
        $sheet->getStyle('A1:H1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        // Lặp qua dữ liệu và đặt giá trị vào các ô tương ứng
        $row = 2; // Dòng bắt đầu từ 2
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $row, $item->full_name);
            $sheet->setCellValue('B' . $row, $item->email);
            $sheet->setCellValue('C' . $row, $item->phone_number);
            $sheet->setCellValue('D' . $row, $item->address);
            $sheet->setCellValue('E' . $row, $item->description);
            $sheet->setCellValue('F' . $row, isset($item->product()->first()->name)? $item->product()->first()->name : 'Sản phẩm đã không còn');
            $datetime = Carbon::parse($item->time);
            $sheet->setCellValue('G' . $row, $datetime->format('d/m/Y'));
            $sheet->setCellValue('H' . $row, $item->created_at->format('d/m/Y'));
            // Đóng khung các ô có dữ liệu
            $sheet->getStyle('A' . $row . ':H' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
            ]);
            $row++;
        }

        // Đặt kích thước font cho hàng còn lại (size 11)
        $sheet->getStyle('A2:H' . $row)->applyFromArray([
            'font' => [
                'size' => 11,
            ],
        ]);

        // Đặt chiều rộng các cột dựa trên chiều dài nội dung
        $lastColumn = $sheet->getHighestColumn();
        $columnIndex = 'A';

        while ($columnIndex <= $lastColumn) {
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }
        $sheet->getStyle('A1:H' . ($row - 1))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        //Tạo filter
        $sheet->setAutoFilter('F1:H' . ($row - 1));

         // Tạo một đối tượng Writer để ghi file Excel
         $writer = new Xlsx($spreadsheet);
         $fileName = 'DangKyLaiThu.' .  $startDate . '.' . $endDate . '.xlsx';

         // Thiết lập header của response để trình duyệt tải xuống file Excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="' . $fileName . '"');
         header('Cache-Control: max-age=0');

         // Ghi file Excel vào output buffer
         $writer->save('php://output');
    }

    function exportQuotation(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = CustomerContact::where('type', CustomerContact::QUOTATION);
        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $data->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $data->whereDate('created_at', '<=', $endDate);
        }
        $data = $data->orderByDESC('created_at')->get();

        // Tạo một đối tượng Spreadsheet
        $spreadsheet = new Spreadsheet();

        // Lấy sheet hiện tại
        $sheet = $spreadsheet->getActiveSheet();

        // Đặt dòng đầu tiên là tiêu đề và định dạng
        $sheet->setCellValue('A1', 'Họ và tên');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'Số điện thoại');
        $sheet->setCellValue('D1', 'Địa chỉ');
        $sheet->setCellValue('E1', 'Nội dung');
        $sheet->setCellValue('F1', 'Sản phẩm');
        $sheet->setCellValue('G1', 'Ngày tạo');


        // Định dạng font cho hàng đầu tiên (size 14, đậm)
        $sheet->getStyle('A1:G1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        // Lặp qua dữ liệu và đặt giá trị vào các ô tương ứng
        $row = 2; // Dòng bắt đầu từ 2
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $row, $item->full_name);
            $sheet->setCellValue('B' . $row, $item->email);
            $sheet->setCellValue('C' . $row, $item->phone_number);
            $sheet->setCellValue('D' . $row, $item->address);
            $sheet->setCellValue('E' . $row, $item->description);
            $sheet->setCellValue('F' . $row, isset($item->product()->first()->name)? $item->product()->first()->name : 'Sản phẩm đã không còn');
            $sheet->setCellValue('G' . $row, $item->created_at->format('d/m/Y'));
            // Đóng khung các ô có dữ liệu
            $sheet->getStyle('A' . $row . ':G' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
            ]);
            $row++;
        }

        // Đặt kích thước font cho hàng còn lại (size 11)
        $sheet->getStyle('A2:G' . $row)->applyFromArray([
            'font' => [
                'size' => 11,
            ],
        ]);

        // Đặt chiều rộng các cột dựa trên chiều dài nội dung
        $lastColumn = $sheet->getHighestColumn();
        $columnIndex = 'A';

        while ($columnIndex <= $lastColumn) {
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }
        $sheet->getStyle('A1:G' . ($row - 1))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        //Tạo filter
        $sheet->setAutoFilter('F1:G' . ($row - 1));

         // Tạo một đối tượng Writer để ghi file Excel
         $writer = new Xlsx($spreadsheet);
         $fileName = 'BaoGia.' .  $startDate . '.' . $endDate . '.xlsx';

         // Thiết lập header của response để trình duyệt tải xuống file Excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="' . $fileName . '"');
         header('Cache-Control: max-age=0');

         // Ghi file Excel vào output buffer
         $writer->save('php://output');
    }
    function exportEbrochure(Request $request)
    {
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = CustomerContact::where('type', CustomerContact::EBROCHURE);
        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $data->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $data->whereDate('created_at', '<=', $endDate);
        }
        $data = $data->orderByDESC('created_at')->get();

        // Tạo một đối tượng Spreadsheet
        $spreadsheet = new Spreadsheet();

        // Lấy sheet hiện tại
        $sheet = $spreadsheet->getActiveSheet();

        // Đặt dòng đầu tiên là tiêu đề và định dạng
        $sheet->setCellValue('A1', 'Họ và tên');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'Số điện thoại');
        $sheet->setCellValue('D1', 'Địa chỉ');
        $sheet->setCellValue('E1', 'Sản phẩm');
        $sheet->setCellValue('F1', 'Ngày tạo');


        // Định dạng font cho hàng đầu tiên (size 14, đậm)
        $sheet->getStyle('A1:F1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        // Lặp qua dữ liệu và đặt giá trị vào các ô tương ứng
        $row = 2; // Dòng bắt đầu từ 2
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $row, $item->full_name);
            $sheet->setCellValue('B' . $row, $item->email);
            $sheet->setCellValue('C' . $row, $item->phone_number);
            $sheet->setCellValue('D' . $row, $item->address);
            $sheet->setCellValue('E' . $row, isset($item->product()->first()->name)? $item->product()->first()->name : 'Sản phẩm đã không còn');
            $sheet->setCellValue('F' . $row, $item->created_at->format('d/m/Y'));
            // Đóng khung các ô có dữ liệu
            $sheet->getStyle('A' . $row . ':F' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
            ]);
            $row++;
        }

        // Đặt kích thước font cho hàng còn lại (size 11)
        $sheet->getStyle('A2:F' . $row)->applyFromArray([
            'font' => [
                'size' => 11,
            ],
        ]);

        // Đặt chiều rộng các cột dựa trên chiều dài nội dung
        $lastColumn = $sheet->getHighestColumn();
        $columnIndex = 'A';

        while ($columnIndex <= $lastColumn) {
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }
        $sheet->getStyle('A1:F' . ($row - 1))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        //Tạo filter
        $sheet->setAutoFilter('E1:F' . ($row - 1));

         // Tạo một đối tượng Writer để ghi file Excel
         $writer = new Xlsx($spreadsheet);
         $fileName = 'TaiEbrochure.' .  $startDate . '.' . $endDate . '.xlsx';

         // Thiết lập header của response để trình duyệt tải xuống file Excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="' . $fileName . '"');
         header('Cache-Control: max-age=0');

         // Ghi file Excel vào output buffer
         $writer->save('php://output');
    }

    function showCredit(Request $request) {
        $query = CustomerContact::where('type', CustomerContact::CREDIT);

        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $query->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $query->whereDate('created_at', '<=', $endDate);
        }

        $customers = $query->orderByDesc('created_at')->paginate(12);
        return view('admin.customer.credit', ['customers' => $customers]);
    }

    function exportCredit(Request $request){
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');

        $data = CustomerContact::where('type', CustomerContact::CREDIT);
        // Lọc theo ngày bắt đầu
        if ($request->has('start_date')  &&  $request->input('start_date')) {
            $startDate = $request->input('start_date');
            $data->whereDate('created_at', '>=', $startDate);
        }

        // Lọc theo ngày kết thúc
        if ($request->has('end_date') &&  $request->input('end_date')) {
            $endDate = $request->input('end_date');
            $data->whereDate('created_at', '<=', $endDate);
        }
        $data = $data->orderByDESC('created_at')->get();

        // Tạo một đối tượng Spreadsheet
        $spreadsheet = new Spreadsheet();

        // Lấy sheet hiện tại
        $sheet = $spreadsheet->getActiveSheet();

        // Đặt dòng đầu tiên là tiêu đề và định dạng
        $sheet->setCellValue('A1', 'Họ và tên');
        $sheet->setCellValue('B1', 'Số điện thoại');
        $sheet->setCellValue('C1', 'Sản phẩm');
        $sheet->setCellValue('D1', 'Ngày tạo');


        // Định dạng font cho hàng đầu tiên (size 14, đậm)
        $sheet->getStyle('A1:D1')->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 14,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        // Lặp qua dữ liệu và đặt giá trị vào các ô tương ứng
        $row = 2; // Dòng bắt đầu từ 2
        foreach ($data as $item) {
            $sheet->setCellValue('A' . $row, $item->full_name);
            $sheet->setCellValue('B' . $row, $item->phone_number);
            $sheet->setCellValue('C' . $row, isset($item->product()->first()->name)? $item->product()->first()->name : 'Sản phẩm đã không còn');
            $sheet->setCellValue('D' . $row, $item->created_at->format('d/m/Y'));
            // Đóng khung các ô có dữ liệu
            $sheet->getStyle('A' . $row . ':D' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                    ],
                ],
            ]);
            $row++;
        }

        // Đặt kích thước font cho hàng còn lại (size 11)
        $sheet->getStyle('A2:D' . $row)->applyFromArray([
            'font' => [
                'size' => 11,
            ],
        ]);

        // Đặt chiều rộng các cột dựa trên chiều dài nội dung
        $lastColumn = $sheet->getHighestColumn();
        $columnIndex = 'A';

        while ($columnIndex <= $lastColumn) {
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            $columnIndex++;
        }
        $sheet->getStyle('A1:D' . ($row - 1))->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ],
        ]);

        //Tạo filter
        $sheet->setAutoFilter('C1:D' . ($row - 1));

         // Tạo một đối tượng Writer để ghi file Excel
         $writer = new Xlsx($spreadsheet);
         $fileName = 'TraGop.' .  $startDate . '.' . $endDate . '.xlsx';

         // Thiết lập header của response để trình duyệt tải xuống file Excel
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="' . $fileName . '"');
         header('Cache-Control: max-age=0');

         // Ghi file Excel vào output buffer
         $writer->save('php://output');
    }
    
    function destroy($id){
          $contact = CustomerContact::where('id', $id)->first();
        if (!$contact) {
            return redirect()->route('admin.customer')->with('fail', 'Xóa thất bại.');
        }
        $contact->delete();
        return redirect()->route('admin.customer')->with('success', 'Đã được xóa thành công.');
    }
}
