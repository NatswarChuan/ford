<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\Table;
use App\Models\Post;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Models\Store;
use Illuminate\Auth\Events\Failed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = ProductService::getProductForEbrochurePage();
        return view('product.product', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $data = Table::groupBy('title')->get('title');
        $tables = [];
        foreach ($data as $value) {
            array_push($tables, $value->title);
        }
        return view('admin.products.create', ['categories' => $categories, 'tables' => $tables]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content' => 'required',
            'price' => 'required:numeric',
            'ebrochure' => 'required|max:20480',
            'category_id' => 'required',
            'colors' => 'required|array',
            'colors.*.image' => 'required|string',
            'colors.*.color_code' => 'required|string',
            'colors.*.name' => 'required|string',
            'tables' => 'required|array',
            'tables.*.title' => 'required|string',
            'tables.*.content' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.create')->with('errors',  $validator->errors()->all());
        }


        $product = new Product();
        $product->name = $request->name;
        $product->version = $request->version;
        $product->content = $request->content;
        $product->price = $request->price;
        $product->status = $request->status ? 1 : 0;
        $product->category_id = $request->category_id;
        $product->genarateSlug();
        if(Product::where('slug', $product->slug)->first()){
            return redirect()->route('admin.product.create')->with('errors',  ['Tên sản phẩm đã tồn tại']);
        }
        // Xử lý file ebrochure
        if ($request->hasFile('ebrochure')) {
            $ebrochure = $request->file('ebrochure');
            $ebrochureName = time() . '_' . $ebrochure->getClientOriginalName();
            $ebrochure->move(public_path('storage/ebrochures'), $ebrochureName);
            $product->ebrochure = $ebrochureName;
        }
        $product->save();

        foreach ($request->tables as  $tableData) {
            $table = new Table();
            $table->title = $tableData['title'];
            $table->content = $tableData['content'];
            $table->product_id = $product->id;
            $table->save();
        }

        foreach ($request->colors as  $colorData) {
            $color = new Color();
            $color->color_code = $colorData['color_code'];
            $color->image = $colorData['image'];
            $color->name = $colorData['name'];
            $color->product_id = $product->id;
            $color->save();
        }

        return redirect()->route('admin.product')->with('success', 'Sản phẩm đã được tạo thành công.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if (!isset($slug)) {
            return abort(400, 'Bad Request');
        }
        $product = ProductService::getProductDetailBySlug($slug);
        if (!$product) {
            return abort(404, 'Not Found');
        }
        return view('product.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            return redirect()->route('admin.product.edit')->with('fail', 'Sửa sản phẩm thất bại.');
        }
        $categories = Category::all();
        $data = Table::groupBy('title')->get('title');
        $tables = [];
        foreach ($data as $value) {
            array_push($tables, $value->title);
        }
        return view('admin.products.edit', ['product' => $product, 'categories' => $categories,'tables'=>$tables]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        // Kiểm tra dữ liệu đầu vào
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required',
            'ebrochure' => 'nullable|file|mimes:pdf',
            'colors' => 'required|array',
            'colors.*.image' => 'required|string',
            'colors.*.color_code' => 'required|string',
            'colors.*.name' => 'required|string',
            'tables' => 'required|array',
            'tables.*.title' => 'required|string',
            'tables.*.content' => 'required|string',
            'content' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.edit',['slug'=>$slug])->with('errors',  $validator->errors()->all());
        }

        $product->name = $request->input('name');
        $product->genarateSlug();
        if(Product::where('slug', $product->slug)->first() && Product::where('slug', $product->slug)->first()->slug != $product->slug){
            return redirect()->route('admin.product.edit',['slug'=>$slug])->with('errors',  ['Tên sản phẩm đã tồn tại']);
        }

        // Xử lý ebrochure nếu có
        if ($request->hasFile('ebrochure')) {
            Storage::disk('public')->delete('ebrochures/'.$product->ebrochure);
           $ebrochure = $request->file('ebrochure');
            $ebrochureName = time() . '_' . $ebrochure->getClientOriginalName();
            $ebrochure->move(public_path('storage/ebrochures'), $ebrochureName);
            $product->ebrochure = $ebrochureName;
        }


        // Cập nhật thông tin sản phẩm
        $product->category_id = $request->input('category_id');
        $product->version = $request->input('version');
        $product->price = $request->input('price');
        $product->status = $request->input('status');
        $product->content = $request->input('content');
        $product->status = $request->status ? 1 : 0;

        $product->save();

        // Cập nhật màu sắc
        $colorsData = $request->input('colors');
        if (is_array($colorsData)) {
            $product->colors()->delete(); // Xóa các màu sắc cũ
            foreach ($colorsData as $colorData) {
                $color = new Color();
                $color->image = $colorData['image'];
                $color->name = $colorData['name'];
                $color->color_code = $colorData['color_code'];
                $product->colors()->save($color);
            }
        }

        // Cập nhật bảng thông số
        $tablesData = $request->input('tables');
        if (is_array($tablesData)) {
            $product->tables()->delete(); // Xóa các bảng thông số cũ
            foreach ($tablesData as $tableData) {
                $table = new Table();
                $table->title = $tableData['title'];
                $table->content = $tableData['content'];
                $product->tables()->save($table);
            }
        }

        return redirect()->route('admin.product')->with('success', 'Sản phẩm đã được cập nhật thành công.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $product = Product::where('slug', $slug)->first();
        if (!$product) {
            return redirect()->route('admin.product')->with('fail', 'Xóa sản phẩm thất bại.');
        }
        $product->colors()->delete();
        $product->tables()->delete();
        $product->delete();
        return redirect()->route('admin.product')->with('success', 'Sản phẩm đã được xóa thành công.');
    }

    public function getProductsBySlugAPI(Request $request)
    {
        if (!isset($request->slug)) {
            return abort(400, 'Bad Request');
        }
        $slug = $request->slug;
        $product = ProductService::getProductBySlug($slug);
        if (!$product) {
            return abort(404, 'Not Found');
        }
        return $product;
    }

    public function ebrochure()
    {
        $categories = CategoryService::getCategoryForEbrochurePage();
        return view('product.ebrochure', ['categories' => $categories]);
    }

    public function downloadEbrochure($slug)
    {
        if (!isset($slug)) {
            return abort(400, 'Bad Request');
        }
        $product = ProductService::getProductBySlug($slug);
        if (!$product) {
            return abort(404, 'Not Found');
        }
        $file = public_path() . "\download\\" . $product->ebrochure;

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return Response::download($file, $product->ebrochure, $headers);
    }

    public function getByProductId($id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $products = $category->products()->paginate(12);
        foreach ($products as $product) {
            $product->setAttribute('category', $product->category()->first());
        }
        return view('admin.products.index', ['products' => $products]);
    }

    public function adminIndex()
    {
        $products = Product::latest()->where('type',Product::NEW)->paginate(12);
        foreach ($products as $product) {
            $product->setAttribute('category', $product->category()->first());
        }
        return view('admin.products.index', ['products' => $products]);
    }

    function getProductsAPI()
    {
        $products = Product::where('status', Product::ACTIVE)->get();
        if (!count($products)) {
            return abort(204, 'No content');
        }

        return $products;
    }

    function productEqual(Request $request, $slug)
    {
        if (!isset($slug)) {
            return abort(400, 'Bad Request');
        }
        $product = Product::where('slug', $slug)->get(['id','name','slug','category_id'])->first();
        if (!$request->session()->has('products') || count($request->session()->get('products',[])) == 0) {
            $request->session()->put('products', [$product->id => $product]);
        } else {
            // Lấy mảng đã có từ session
            $data = $request->session()->get('products',[]);
            if(current($data)->category()->first()->id == $product->category()->first()->id) {
                // Mảng mới cần merge
                $data[$product->id] = $product;

                // Merge mảng mới vào mảng đã có trong session

                // Lưu trữ mảng đã merge vào session
                $request->session()->put('products', $data);
            }else{
                $request->session()->put('products', [$product->id => $product]);
            }
        }
        $data = $request->session()->get('products');
        return redirect()->route('product.equal.page');
    }

    function productEqualPage(Request $request)
    {
        $products = $request->session()->get('products',[]);
        if(!$products ){
            return redirect()->route('home');
        }

        if(count($products) < 3){
            $step = 3-count($products);
            for ($i=0; $i < $step; $i++) {
                array_push($products,false);
            }
        }
        return view('product.equal', ['products' => $products]);
    }

    function removeProductEqual(Request $request, $id)
    {
        if ($request->session()->has('products')) {
            $data = $request->session()->get('products',[]);
            unset($data[$id]);
            $request->session()->put('products', $data);
        }
        $data = $request->session()->get('products');
        return redirect()->route('product.equal.page');
    }

    function oldCar(){
        $products = Product::where('type',Product::OLD)->where('status',Product::ACTIVE)->paginate(9);
        return view('product.oldCar', ['products' => $products]);
    }

    function credit(){
        $products = Product::where('type',Product::NEW)->where('status',Product::ACTIVE)->get();
        $post = Post::where('type',Post::CREDIT)->where('status',Post::ACTIVE)->firstOrFail();
                $store = Store::all()->first();
        return view('product.credit', ['products' => $products,'post'=>$post,'store' => $store]);
    }

    function adminOldCar(){
        $products = Product::latest()->where('type',Product::OLD)->paginate(12);
        foreach ($products as $product) {
            $product->setAttribute('category', $product->category()->first());
        }
        return view('admin.products.oldCar', ['products' => $products]);
    }

    public function storeOldCar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'content' => 'required',
            'price' => 'required:numeric',
            'colors' => 'required|array',
            'colors.*.image' => 'required|string',
            'colors.*.color_code' => 'required|string',
            'colors.*.name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.oldCarCreate')->with('errors',  $validator->errors()->all());
        }


        $product = new Product();
        $product->name = $request->name;
        $product->version = $request->version;
        $product->content = $request->content;
        $product->price = $request->price;
        $product->status = $request->status ? 1 : 0;
        $product->type = Product::OLD;
        $product->genarateSlug();
        if(Product::where('slug', $product->slug)->first()){
            return redirect()->route('admin.product.oldCarCreate')->with('errors',  ['Tên sản phẩm đã tồn tại']);
        }
        $product->save();
        foreach ($request->colors as  $colorData) {
            $color = new Color();
            $color->color_code = $colorData['color_code'];
            $color->image = $colorData['image'];
            $color->name = $colorData['name'];
            $color->product_id = $product->id;
            $color->save();
        }

        return redirect()->route('admin.product.oldCar')->with('success', 'Sản phẩm đã được tạo thành công.');
    }

    public function createOldCar()
    {
        $categories = Category::all();
        return view('admin.products.oldCarCreate', ['categories' => $categories]);
    }

    function editOldCar($slug)
    {
        $product = Product::where('slug', $slug)->where('type',Product::OLD)->first();
        if (!$product) {
            return redirect()->route('admin.product.oldCarEdit')->with('fail', 'Sửa sản phẩm thất bại.');
        }
        $categories = Category::all();

        return view('admin.products.oldCarEdit', ['product' => $product, 'categories' => $categories]);
    }

    public function updateOldCar(Request $request, $slug)
    {
        $product = Product::where('slug', $slug)->where('type',Product::OLD)->firstOrFail();
        // Kiểm tra dữ liệu đầu vào
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'price' => 'required',
            'colors' => 'required|array',
            'colors.*.image' => 'required|string',
            'colors.*.color_code' => 'required|string',
            'colors.*.name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.product.oldCarEdit',['slug'=>$slug])->with('errors',  $validator->errors()->all());
        }

        $product->name = $request->input('name');
        $product->genarateSlug();
        if(Product::where('slug', $product->slug)->first() && Product::where('slug', $product->slug)->first()->slug != $product->slug){
            return redirect()->route('admin.product.oldCarEdit',['slug'=>$slug])->with('errors',  ['Tên sản phẩm đã tồn tại']);
        }

        // Cập nhật thông tin sản phẩm
        $product->version = $request->input('version');
        $product->price = $request->input('price');
        $product->status = $request->input('status');
        $product->content = $request->input('content');
        $product->status = $request->status ? 1 : 0;

        $product->save();

        // Cập nhật màu sắc
        $colorsData = $request->input('colors');
        if (is_array($colorsData)) {
            $product->colors()->delete(); // Xóa các màu sắc cũ
            foreach ($colorsData as $colorData) {
                $color = new Color();
                $color->image = $colorData['image'];
                $color->name = $colorData['name'];
                $color->color_code = $colorData['color_code'];
                $product->colors()->save($color);
            }
        }

        return redirect()->route('admin.product.oldCar')->with('success', 'Sản phẩm đã được cập nhật thành công.');
    }

    function getOldCarBySlug($slug){
        $product = Product::where('slug', $slug)->where('type',Product::OLD)->firstOrFail();
        return view('product.oldCarShow',['product' => $product]);
    }
}
