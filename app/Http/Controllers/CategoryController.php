<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PDO;

class CategoryController extends Controller
{
    public function adminIndex()
    {
        $categories = Category::latest()->paginate(12);

        return view('admin.category.index', ['categories' => $categories]);
    }

    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();
        if (!$category) {
            return redirect()->route('admin.category')->with('fail', 'Xóa dòng sản phẩm thất bại.');
        }
        $category->delete();

        return redirect()->route('admin.category')->with('success', 'Dòng sản phẩm đã được xóa thành công.');
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.category')->with('errors',  $validator->errors()->all());
        }

        $post = Category::where('id', $id)->firstOrFail();
        $post->name = $request->name;
        $post->status = $request->status ? 1 : 0;
        $post->save();

        return redirect()->route('admin.category')->with('success', 'Dòng sản phẩm đã được sửa thành công.');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.category')->with('errors',  $validator->errors()->all());
        }

        $post = new Category();
        $post->name = $request->name;
        $post->status = $request->status ? 1 : 0;
        $post->save();

        return redirect()->route('admin.category')->with('success', 'Thêm dòng sản phẩm đã được sửa thành công.');
    }
}
