<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use App\Models\Post;
use App\Services\FileService;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('type',Post::POST)->orderByDesc('published_date')->paginate(6);
        foreach ($posts as $post) {
            $post->setAttribute('shortDescription', $post->getShortDescriptionAttribute());
        }
        $postsNew = PostService::getPostsNewForHomePage();
        return view('posts.index', [
            'posts' => $posts,
            'postsNew' => $postsNew,
        ]);
    }

    public function adminIndex()
    {
        $posts = Post::latest()->paginate(12);
        foreach ($posts as $post) {
            $post->setAttribute('user', $post->user()->first());
        }
        return view('admin.posts.index', [
            'posts' => $posts,
        ]);
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'published_date' => 'required|date',
            'type' => 'required|numeric|min:1|max:3',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.post.create')->with('errors',  $validator->errors()->all());
        }

        $postCheck = Post::where('type', Post::SERVICE)->where('status', Post::ACTIVE)->first();
        if (!!$postCheck  && $request->status && $request->type == Post::SERVICE) {
            return redirect()->route('admin.post.create')->with('fail', 'Đã tồn tại bài viết thuộc loại dịch vụ đang được đăng.');
        }

        $postCheck = Post::where('type', Post::CREDIT)->where('status', Post::ACTIVE)->first();
        if (!!$postCheck && $postCheck->id  && $request->status && $request->type == Post::CREDIT) {
            return redirect()->route('admin.post.create')->with('fail', 'Đã tồn tại bài viết thuộc loại trả góp đang được đăng.');
        }

        $post = new Post();
        $post->title = $request->title;
        $post->genarateSlug();
        $slug = isset($post->slug) ? $post->slug : '';
        if(Post::where('slug',$slug)->first()){
            return redirect()->route('admin.post.create')->with('fail', 'Đã tồn tại bài viết có tên này');
        }

        $post->title = $request->title;
        $post->content = $request->content;
        $post->published_date = $request->published_date;
        $post->type = $request->type;
        $post->user_id = Auth::user()->id;
        $post->image = $request->image;
        $post->status = $request->status ? 1 : 0;
        $post->save();

        return redirect()->route('admin.post')->with('success', 'Bài viết đã được thêm thành công.');
    }

    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        if (!$post) {
            return abort(404);
        }
        $postsNew = PostService::getPostsNewForHomePage();
        return view('posts.show', [
            'post' => $post,
            'postsNew' => $postsNew,
        ]);
    }

    public function edit($slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) {
            return redirect()->route('admin.post.edit')->with('fail', 'Sửa bài viết thất bại.');
        }

        return view('admin.posts.edit', ['post' => $post]);
    }

    public function update(Request $request, $slug)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'published_date' => 'required|date',
            'type' => 'required|numeric|min:1|max:3',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.post.edit', $slug)->with('errors',  $validator->errors()->all());
        }

        $post = Post::where('slug', $slug)->firstOrFail();
        $postCheck = Post::where('type', Post::SERVICE)->where('status', Post::ACTIVE)->first();
        if (!!$postCheck && $postCheck->id != $post->id  && $request->status && $request->type == Post::SERVICE) {
            return redirect()->route('admin.post.edit', $slug)->with('fail', 'Đã tồn tại bài viết thuộc loại dịch vụ đang được đăng.');
        }

        $postCheck = Post::where('type', Post::CREDIT)->where('status', Post::ACTIVE)->first();
        if (!!$postCheck && $postCheck->id != $post->id  && $request->status && $request->type == Post::CREDIT) {
            return redirect()->route('admin.post.edit', $slug)->with('fail', 'Đã tồn tại bài viết thuộc loại trả góp đang được đăng.');
        }
        if(Post::where('slug',$post->slug)->first()&& Post::where('slug',$post->slug)->first()->slug != $post->slug){
            return redirect()->route('admin.post.create')->with('fail', 'Đã tồn tại bài viết có tên này');
        }
        $post->title = $request->title;
        $post->content = $request->content;
        $post->image = $request->image;
        $post->published_date = $request->published_date;
        $post->type = $request->type;
        $post->status = $request->status ? 1 : 0;
        $post->save();

        return redirect()->route('admin.post')->with('success', 'Bài viết đã được cập nhật thành công.');
    }

    public function destroy($slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) {
            return redirect()->route('admin.post')->with('fail', 'Xóa bài viết thất bại.');
        }
        $post->delete();

        return redirect()->route('admin.post')->with('success', 'Bài viết đã được xóa thành công.');
    }
}
