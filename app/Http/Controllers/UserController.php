<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'role' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin')->with('errors',  $validator->errors()->all());
        }
        $auth = Auth::user();
        if ($auth->role == User::EDIT_USER || ($auth->role == User::ADMIN_USER && $request->input('role') == User::ADMIN_USER && $request->input('role') == User::MASTER_USER) || $request->input('role') == User::MASTER_USER) {
            return redirect()->route('admin')->with('fail',  "Bạn không đủ quyền");
        }
        // Tạo người dùng mới trong cơ sở dữ liệu
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->password = bcrypt(User::DEFAULT_PASSWORD);
        $user->save();

        // Chuyển hướng người dùng sau khi tạo thành công
        return redirect()->route('admin')->with('success', 'Người dùng đã được tạo thành công!');
    }

    public function destroy($id)
    {
        // Tìm người dùng theo ID
        $user = User::find($id);

        if (!$user) {
            return redirect()->route('admin')->with('fail', 'Người dùng không tồn tại!');
        }

        // Kiểm tra quyền và xác định quyền xóa
        $auth = Auth::user();
        if ($auth == User::EDIT_USER) {
            return redirect()->route('admin')->with('fail', 'Bạn không có quyền xóa người dùng!');
        } elseif ($auth == User::ADMIN_USER && ($user->role === User::ADMIN_USER || $user->role === User::MASTER_USER)) {
            return redirect()->route('admin')->with('fail', 'Bạn không có quyền xóa người dùng này!');
        }

        // Xóa người dùng
        $user->delete();

        return redirect()->route('admin')->with('success', 'Người dùng đã được xóa thành công!');
    }
    function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|string|min:8',
            'confirm_password' => 'required|same:new_password',
        ]);
    
        if ($validator->fails()) {
            return redirect()->back()->with('errors',  $validator->errors()->all());
        }

        // Kiểm tra xác thực người dùng hiện tại
        $user = Auth::user();

        // Kiểm tra mật khẩu hiện tại
        if (!Hash::check($request->current_password, $user->password)) {
            return redirect()->route('admin')->with('fails', 'Mật khẩu hiện tại không chính xác');
        }

        // Kiểm tra mật khẩu mới và xác nhận mật khẩu
        if ($request->new_password !== $request->confirm_password) {
            return redirect()->route('admin')->with('fails', 'Mật khẩu mới và xác nhận mật khẩu không khớp');
        }
        $user = User::find($user->id);
        // Cập nhật mật khẩu mới
        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect()->route('admin')->with('success', 'Mật khẩu đã được thay đổi thành công');
    }
}
