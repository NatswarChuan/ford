<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'iframe' => 'required',
            'contact' => 'required',
            'credit' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin')->with('errors',  $validator->errors()->all());
        }

        $store = Store::find(1);
        $store->name = $request->name;
        $store->address = $request->address;
        $store->email = $request->email;
        $store->credit = $request->credit;
        $store->contact = $request->contact;
        if($store->iframe != $request->iframe){
           $store->iframe =  str_replace("<iframe ", "<iframe class=\"w-100 h-100\" ", $request->iframe) ; 
        }
        
        $store->phone = $request->phone;
        $store->save();

        return redirect()->route('admin')->with('success', 'Cập nhật thông tin thành công');
    }
}
