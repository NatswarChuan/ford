<?php

namespace App\Http\Controllers;

use App\Models\Iframe;
use App\Models\Post;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Store;
use App\Models\User;
use App\Services\CategoryService;
use App\Services\PostService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PageController extends Controller
{
    public function index()
    {
        $categories = CategoryService::getCategoryForHomePage();
        $sliders = Slider::all();
        $postsNew = PostService::getPostsNewForHomePage();
        $postsHot = PostService::getPostsSortByViewsForHomePage();
        $store = Store::first();
        $slug = $categories->first()->products->first()->slug;
        return view('home.index', [
            'categories' => $categories,
            'sliders' => $sliders,
            'postsNew' => $postsNew,
            'postsHot' => $postsHot,
            'store' => $store,
            'slug' => $slug
        ]);
    }

    public function contact()
    {
        return Store::first()->toArray();
    }

    public function service()
    {
        $post = PostService::getService();
        // if (!$post) {
        //     return abort(404);
        // }
        $store = Store::all()->first();

        return view('posts.show', [
            'post' => $post,
            '$store' => $store,
        ]);
    }

    public function aboutUs()
    {
        $store = Store::first();
        return view('lienhe', [
            'store' => $store
        ]);
    }

    public function admin()
    {
        $store = Store::first();
        $users = User::all();
        $auth = Auth::user();
        return view('admin.index', ['store' => $store, 'users' => $users, 'auth' => $auth]);
    }

    public function changeGallery()
    {
        $sliders = Slider::all();
        return view('admin.slider', ['sliders' => $sliders]);
    }

    function destroyGallery($id)
    {
        $slider =  Slider::find($id);
        if (!$slider) {
            return redirect()->back()->with('fail', 'Hình ảnh đã bị xóa');
        }
        $slider->delete();
        return redirect()->back()->with('success', 'Xóa thành công!');
    }

    function storeGallery(Request $request)
    {
        $slider = new Slider();
        $slider->image = $request->image;
        $slider->save();
        return redirect()->back()->with('success', 'Thêm thành công!');
    }

    function changeIcon(Request $request)  {
        // Kiểm tra nếu tệp đã được gửi lên
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');

            // Đường dẫn đến thư mục public/images và tên tệp mới
            $destinationPath = public_path('images');
            $fileName = 'logo.png';

            // Kiểm tra xem tệp cũ đã tồn tại
            if (File::exists($destinationPath . '/' . $fileName)) {
                // Xóa tệp cũ
                File::delete($destinationPath . '/' . $fileName);
            }

            // Di chuyển tệp mới vào thư mục public/images
            $file->move($destinationPath, $fileName);

            // Thông báo thành công nếu tệp đã được lưu thành công
            return redirect()->back()->with(['success' => 'Icon đã được cập nhật thành công']);
        }

        // Trả về thông báo lỗi nếu không có tệp nào được gửi lên
        return redirect()->back()->with(['fail' => 'Không tìm thấy tệp icon']);
    }
}
