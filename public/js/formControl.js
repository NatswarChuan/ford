const formControl = (() => {
  /**
   *
   * @param {Node} _form
   * @returns {Value[]}
   */
  const getValue = (_form) => {
    const form = _form
    let result = {}

    form.querySelectorAll("input").forEach(e => {
      result = {
        ...result,
        [e.getAttribute("name")]: e.value
      }
    });

    form.querySelectorAll("textarea").forEach(e => {
      result = {
        ...result,
        [e.getAttribute("name")]: e.value
      }
    });

    form.querySelectorAll("select").forEach(e => {
      result = {
        ...result,
        [e.getAttribute("name")]: e.value
      }
    });

    return result;
  }

  /**
   * set id into hidden field
   * @param {String} id value of id
   * @param {String} name name of id field
   * @param {Node} _form nullable
   */
  function setId(id, name, _form) {
    const form = _form
    form.querySelector(`[name="${name}"]`).value = id
  }

  /**
   * hàm để gửi api form
   * @param {String} url đường dẫn đến api
   * @param {Function} thenFunc hàm khi trả về thành công
   * @param {Function} catchFunc hàm khi gọi đến lỗi
   * @param {Node} _form node form muốn submit
   * @returns {Function} sự kiện click button submit
   */
  function submitForm(url, thenFunc, catchFunc, _form) {
    const form = _form

    const eventFunc = (e) => {
      window.axios.post(url, getValue(form))
        .then(thenFunc)
        .catch(catchFunc);
    }

    form.addEventListener("submit", eventFunc)

    return eventFunc;
  }

  function removeSubmitForm(eventFunc,_form) {
    const form = _form

    const btnSubmit = form.querySelector("[type='submit']");

    btnSubmit.removeEventListener("click", eventFunc);
  }

  return {
    getValue,
    setId,
    submitForm
  }
})()
