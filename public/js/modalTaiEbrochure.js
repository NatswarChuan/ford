(() => {
  const formTaiEbrochure = document.querySelector("#modal-tai-ebrochure");

  document.querySelectorAll('.button-tai-ebrochure').forEach(element => {
    element.addEventListener("click", (e) => {
      formControl.setId(element.getAttribute("data-id"), "product_id", formTaiEbrochure);

      formControl.submitForm(urlTaiEbrochure, (e) => {
        $("[data-bs-dismiss='modal']").click()
        window.open(e.data.link);
      }, (err) => {
        alertControl.showAlert("alert-container-tai-ebrochure", Object.values(err.response.data.errors).join('<br/>'), "danger", formTaiEbrochure);
      }, formTaiEbrochure)
    })
  });
})()


