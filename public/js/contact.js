(() => {
  const lastName = document.querySelector('[name="last_name"]');
  const firstName = document.querySelector('[name="first_name"]');
  const phone = document.querySelector('[name="phone"]');
  const address = document.querySelector('[name="address"]');
  var myToastEl = document.querySelector('.toast')
  var myToast = bootstrap.Toast.getOrCreateInstance(myToastEl)

  document.querySelector('#form-contact').addEventListener("submit", (e) => {
    e.preventDefault();

    if (lastName.value + firstName.value === "") {
      document.querySelector(".toast-body").innerHTML = "Name cannot empty";
      myToast.show();
      return;
    }
    if (phone.value === "") {
      document.querySelector(".toast-body").innerHTML = "Phone cannot empty";
      myToast.show();
      return;
    }
    if (address.value === "") {
      document.querySelector(".toast-body").innerHTML = "Address cannot empty";
      myToast.show();
      return;
    }

    const data = {
      full_name: lastName.value + firstName.value,
      phone_number: phone.value,
      address: address.value,
    };

    // Gửi yêu cầu POST
    window.axios.post(baseURL+'api/thong-tin-khach-hang', data)
      .then(response => {
          location.replace(baseURL);
      })
      .catch(error => {
        document.querySelector(".toast-body").innerHTML = error;
        myToast.show();
      });

    
  })
})()
