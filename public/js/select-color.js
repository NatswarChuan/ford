

const getProducts = document.querySelectorAll(".get-product");

getProducts.forEach(element => {
  element.addEventListener('click', (e) => {
    getProduct(e.target.getAttribute("data-slug"));
  })
});

function getProduct(slug) {
  const productContent = document.querySelector('.product-content');
  window.axios.get(`/api/products/${slug}`)
    .then(function (response) {
      const data = response.data;

      productContent.querySelector(".name").innerHTML = data.name;
      productContent.querySelector(".name").setAttribute("href",`${baseURL}chi-tiet-san-pham/${slug}`);
      productContent.querySelector(".price").innerHTML = data.price + " VNĐ";

      productContent.querySelector(".black-button").setAttribute("href",`${baseURL}chi-tiet-san-pham/${slug}`);

      productContent.querySelector(".image-car").innerHTML = ""
      productContent.querySelector(".image-car").setAttribute("href",`${baseURL}chi-tiet-san-pham/${slug}`);
      productContent.querySelector(".color-list").innerHTML = ""

      data.colors.forEach((color, index) => {
        productContent.querySelector(".color-list").innerHTML += `<span class="swiper-pagination- bullet color-pick custom-cursor-on-hover ${index == 0 ? "swiper-pagination-bullet-active" : ""}" data-color-id="${color.id}"
        style="--data-color:${color.color_code}"></span>`

        productContent.querySelector(".image-car").innerHTML += `<img class="img-fluid image-car-pick ${index == 0 ? "" : "hidden"}" data-color-id="${color.id}" src="${baseURL + "storage/" + color.image}"
          alt="Seltos" style="max-width: 80%!important;">`
      });

      const colorPick = productContent.querySelectorAll(".color-pick");
      const imageCar = productContent.querySelector(".image-car");

      colorPick.forEach(colorElement => {
        colorElement.addEventListener('click', (e) => {
          colorPick.forEach(colorP => {
            colorP.classList.remove("swiper-pagination-bullet-active");
          })
          document.querySelectorAll('.image-car-pick').forEach((imageCarPick) => {
            imageCarPick.classList.remove("hidden");
            imageCarPick.classList.add("hidden");
          })
          e.target.classList.add("swiper-pagination-bullet-active")
          imageCar.querySelector(`[data-color-id="${e.target.getAttribute("data-color-id")}"]`).classList.remove("hidden");
        })
      });
    })
    .catch(function (error) {
      console.log(error);
    });
}
