let flagAvatar = null;
let flagDes = null;
const addImage = document.getElementById('addImage');
const imageInput = document.getElementById('imageInput');
addImage.addEventListener('click', function () {
    imageInput.click();
})
imageInput.addEventListener('change', function () {
    var file = imageInput.files[0];
    if (file) {
        var formData = new FormData();
        formData.append('image', file);
        window.axios.post('/api/images/uploads', formData)
            .then(function (response) {
                alert('Thêm ảnh thành công');
                getImages();
            })
            .catch(function (error) {
                console.error('Lỗi khi tải lên hình ảnh:', error);
            });
    } else {
        console.log('Vui lòng chọn một hình ảnh trước khi gửi.');
    }
});
const modal = new bootstrap.Modal(document.getElementById('imageModal'));

function selectImage(imageUrl) {
    var image = document.getElementById('image');
    var preview = document.getElementById('preview');
    preview.src = imageUrl;
    preview.style.display = 'block';
    image.value = imageUrl.replace('/storage/', '');
}

function previewImage(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var preview = document.getElementById('preview');
        preview.src = reader.result;
        preview.style.display = 'block';
    }
    reader.readAsDataURL(event.target.files[0]);
}

function getImages() {
    window.axios.get('/api/images')
        .then(function (response) {
            var images = response.data.images;
            var imageList = document.getElementById('imageList');
            imageList.innerHTML = '';
            images.forEach(function (image) {
                var imageUrl = '/storage/' + image;
                var imgElement = document.createElement('div');
                imgElement.innerHTML =
                    `
<img class="img-fluid p-1" src="${imageUrl}" style="cursor:pointer;" title="${imageUrl.replace('/storage/', '')}">`;
                imgElement.querySelector('img').onclick = function () {
                    let docModal = document.getElementById('optionModal');
                    var optionModal = new bootstrap.Modal(document.getElementById('optionModal'));
                    optionModal.show();
                    docModal.querySelector('.btn-primary').onclick = () => {
                        if (flagAvatar != null && flagAvatar) {
                            selectImage(imageUrl);
                        } else if (flagDes != null && flagDes) {
                            descriptionImage(image);
                        } else {
                            chosenImage(imageUrl);
                        }
                        optionModal.hide();
                        modal.hide();
                    }
                    docModal.querySelector('.btn-danger').onclick = () => {
                        removeImage(image);
                        optionModal.hide();
                    }
                };
                imageList.appendChild(imgElement);
            });

        })
        .catch(function (error) {
            alert('Lỗi không xác định:', error);
        });
}

function removeImage(image) {
    var result = confirm("Bạn có chắc chắn muốn xóa hình ảnh?");
    if (result) {
        window.axios.post('/api/images/remove', {
            image
        })
            .then(function (response) {
                alert('Xóa ảnh thành công');
                getImages();
            })
            .catch(function (error) {
                if (error.response && error.response.status === 400) {
                    alert(error.response.data.error)
                } else {
                    alert('Lỗi không xác định:', error);
                }
            });
    }

}