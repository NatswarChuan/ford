(() => {
  const formDangKyLaiThu = document.querySelector("#modal-contact");

  if (!localStorage.getItem('modalContact')) {
    localStorage.setItem('modalContact', true);

    setTimeout(() => {
      $("#modal-contact").modal("show")

      formControl.submitForm(thongTinKhachHang, (e) => {
        $("[data-bs-dismiss='modal']").click()
      }, (err) => {
        alertControl.showAlert("alert-container-contact", Object.values(err.response.data.errors).join('<br/>'), "danger", formDangKyLaiThu);
      }, formDangKyLaiThu)
    }, 15000);
  }
})()
