window.axios.get(`/api/contact`)
  .then(function (response) {
    const data = response.data;

    const contact = document.querySelector(".contact");

    contact.querySelector('.contact-name').innerHTML += data.name;
    contact.querySelector('.contact-address').innerHTML += data.address;
    contact.querySelector('.phone').innerHTML += data.phone;
    contact.querySelector('.email').innerHTML += data.email;
    contact.querySelector('#menuDesktop').innerHTML += data.iframe;
    document.querySelector('.phone-btn').setAttribute("href", "tel:"+data.phone.split('-')[0]);
    document.querySelector('#linkzalo').setAttribute("href", "https://zalo.me/"+data.phone.split('-')[0] );
    // if ( window.screen.width < 1200) { document.getElementById("linkzalo").setAttribute("href","https://zalo.me/"+data.phone.split('-')[0]); }
  })
  .catch(function (error) {
    console.log(error);
  });
