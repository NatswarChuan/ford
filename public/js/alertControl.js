const alertControl = (() => {
  /**
   * show alert
   * @param {String} container id alert container
   * @param {String} message thông báo hiển thị
   * @param {String} type kiểu thông báo xem ở bootstrap vd(danger,success,...)
   * @returns
   */
  function showAlert(container, message, type) {
    const alertContainer = document.getElementById(container);
    const alert = document.createElement('div');
    alert.classList.add('alert', `alert-${type}`);
    alert.innerHTML = message;
    alertContainer.appendChild(alert);

    return new bootstrap.Alert(alert)
  }

  return {
    showAlert
  }
})()
