var quill = new Quill('#description', {
    theme: 'snow',
    modules: {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{
                'header': [1, 2, 3, 4, 5, 6, false]
            }],
            [{
                'list': 'ordered'
            }, {
                'list': 'bullet'
            }],
            [{
                'script': 'sub'
            }, {
                'script': 'super'
            }],
            [{
                'indent': '-1'
            }, {
                'indent': '+1'
            }],
            [{
                'direction': 'rtl'
            }],
            [{
                'align': []
            }],
            ['link', 'image', 'video'],
            ['clean']
        ]
    },
    placeholder: 'Nhập nội dung ở đây...'
});


const description = document.querySelector('.ql-editor').innerHTML;

document.querySelector('#form').addEventListener('submit', function (e) {
    const description = document.querySelector('.ql-editor').innerHTML;
    const content = document.querySelector('#description-content');
    content.value = '<div class="ql-snow"><div class="ql-editor">'+description+'</div></div>';
});

function previewImage(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var preview = document.getElementById('preview');
        preview.src = reader.result;
        preview.style.display = 'block';
    }
    reader.readAsDataURL(event.target.files[0]);
    event.target.value = event.target.files[0];
}

function descriptionImage(image) {
    flagDes = false;
    quill.insertEmbed(quill.getSelection().index, 'image', '/storage/' + image);
}
var toolbar = quill.getModule('toolbar');

toolbar.addHandler('image', (e) => {
    flagAvatar = false;
    flagDes = true;
    getImages();
    modal.show();
});
