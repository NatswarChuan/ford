// Slideshow logic
const slides = document.getElementsByClassName("mySlides");
const dots = document.getElementsByClassName("dot");

setTimeout(() => {
  let slideIndex = 1;

  showSlide(slideIndex);
}, 0);

function showSlide(slideIndex) {
  showSlides(slideIndex)
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1
  }
  setTimeout(() => {
    showSlide(slideIndex)
  }, 3000);
}

function showSlides(slideIndex) {
  for (let i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  slides[slideIndex - 1].style.display = "block";
}
